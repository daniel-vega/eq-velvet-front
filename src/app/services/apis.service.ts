import { Injectable } from '@angular/core';
// import { AngularFireAuth } from '@angular/fire/auth';
import { BehaviorSubject } from 'rxjs';
// import * as firebase from 'firebase';
// import { AngularFirestore } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import { resolve } from 'dns';

export class AuthInfo {
  constructor(public $uid: string) {}

  isLoggedIn() {
    return !!this.$uid;
  }
}

@Injectable({
  providedIn: 'root',
})
export class ApisService {
  static UNKNOWN_USER = new AuthInfo(null);
  // db = firebase.firestore();
  public authInfo$: BehaviorSubject<AuthInfo> = new BehaviorSubject<AuthInfo>(
    ApisService.UNKNOWN_USER
  );
  constructor(private http: HttpClient) {}
  public login(email: string, password: string): Promise<any> {
    //   return new Promise<any>((resolve, reject) => {
    //     this.fireAuth.auth.signInWithEmailAndPassword(email, password)
    //       .then(res => {
    //         if (res.user) {
    //           this.db.collection('users').doc(res.user.uid).update({
    //             fcm_token: localStorage.getItem('fcm') ? localStorage.getItem('fcm') : '',
    //           });
    //           this.authInfo$.next(new AuthInfo(res.user.uid));
    //           resolve(res.user);
    //         }
    //       })
    //       .catch(err => {

    //         this.authInfo$.next(ApisService.UNKNOWN_USER);
    //         reject(`login failed ${err}`);
    //       });
    //   });
    // }
    return new Promise<any>((resolve, reject) => {
      if (email === 'admin@gmail.com' && password === '123456') resolve(true);
      else resolve(false);
    });
  }
}
