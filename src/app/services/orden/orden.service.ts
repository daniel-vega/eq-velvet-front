import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs/internal/observable/throwError';
import { SubirArchivoService } from '../subir-archivo/subir-archivo.service';
import { AlertsComponent } from 'src/app/components/alerts/alerts.component';
import { environment } from 'src/environments/environment';
import { Ordenes } from '../../interfaces/IOrdenes.interface';
@Injectable({
  providedIn: 'root',
})
export class OrdenService {
  private readonly URL_SERVICIOS = environment.URL_ENDPOINT + '/orden';
  constructor(
    private _http: HttpClient,
    private _subirArchivoService: SubirArchivoService,
    private _alertsComponent: AlertsComponent
  ) {}

  public crearOrden(ordenPost: any) {
    const url = this.URL_SERVICIOS + '/registrar';
    debugger;
    return this._http.post(url, ordenPost).pipe(
      map((res: any) => {
        return res;
      }),
      catchError((err) => {
        this._alertsComponent.alertaInformativa(err.message);
        return throwError(err);
      })
    );
  }
  public actualizarOrden(usuario: Ordenes) {}
  public getOrdenes() {
    const url = this.URL_SERVICIOS + '/listar';
    return this._http.get(`${url}`).pipe(
      map((res: any) => {
        return res;
      }),
      catchError((err) => {
        this._alertsComponent.alertaInformativa(err.message);
        return throwError(err);
      })
    );
  }
  public getAllOrdenesByID(idUsuario) {
    debugger;
    const url = this.URL_SERVICIOS + '/listarcliente';
    return this._http.get(`${url}/${idUsuario}`).pipe(
      map((res: any) => {
        return res;
      }),
      catchError((err) => {
        this._alertsComponent.alertaInformativa(err.message);
        return throwError(err);
      })
    );
  }

  public getOrdenesNoEntrgadasByID(idUsuario) {
    const url = this.URL_SERVICIOS + '/ordenesNoEntregadas/cliente';
    return this._http.get(`${url}/${idUsuario}`).pipe(
      map((res: any) => {
        return res;
      }),
      catchError((err) => {
        this._alertsComponent.alertaInformativa(err.message);
        return throwError(err);
      })
    );
  }
  public getOrdenesNoEntrgadas() {
    const url = this.URL_SERVICIOS + '/ordenesNoEntregadas';
    return this._http.get(`${url}`).pipe(
      map((res: any) => {
        return res;
      }),
      catchError((err) => {
        this._alertsComponent.alertaInformativa(err.message);
        return throwError(err);
      })
    );
  }

  public getOrdenesDetalleByID(idOrdenDetalle: number, idDireccion: number) {
    const url = this.URL_SERVICIOS + '/detalleorden';
    return this._http.get(`${url}/${idOrdenDetalle}/${idDireccion}/1`).pipe(
      map((res: any) => {
        return res;
      }),
      catchError((err) => {
        this._alertsComponent.alertaInformativa(err.message);
        return throwError(err);
      })
    );
  }
  public buscarOrdenes(termino: Ordenes) {}
}
