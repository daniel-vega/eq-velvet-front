import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs/internal/observable/throwError';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { SubirArchivoService } from '../subir-archivo/subir-archivo.service';
import { Usuario } from '../../interfaces/IUsuarios.interface';
import { AlertsComponent } from 'src/app/components/alerts/alerts.component';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class UsuarioService {
  private readonly URL_SERVICIOS = environment.URL_ENDPOINT + '/usuario';
  usuario: Usuario;
  token: string;
  menu: any[] = [];
  constructor(
    private _http: HttpClient,
    private _router: Router,
    private _alertsComponent: AlertsComponent
  ) {
    this.cargarStorage();
  }
  public renuevaToken() {}
  public estaLogueado() {
    return this.token.length > 5 ? true : false;
  }
  public cargarStorage() {
    if (localStorage.getItem('token')) {
      this.token = localStorage.getItem('token');
      this.usuario = JSON.parse(localStorage.getItem('usuario'));
      this.menu = JSON.parse(localStorage.getItem('menu'));
    } else {
      this.token = '';
      this.usuario = null;
      this.menu = [];
    }
  }
  public guardarStorage(token: string, usuario: Usuario) {
    localStorage.setItem('token', token);
    localStorage.setItem('usuario', JSON.stringify(usuario));
    this.usuario = usuario;
    this.token = token;
  }

  public logout() {
    this.usuario = null;
    this.token = '';
    this.menu = [];
    localStorage.removeItem('token');
    localStorage.removeItem('usuario');
    localStorage.clear();
    this._router.navigate(['/login']);
  }

  public login(email: string, contrasenia: string, recordar: boolean = false) {
    const url = this.URL_SERVICIOS + '/login';
    if (recordar) {
      localStorage.setItem('email', email);
    } else {
      localStorage.removeItem('email');
    }

    let usuarioAccess = { email, contrasenia };
    return this._http.post(url, usuarioAccess).pipe(
      map((res: any) => {
        this.guardarStorage(res.jwt, res.usuario);
        return res.usuario;
      }),
      catchError((err) => {
        debugger;
        this._alertsComponent.alertaInformativa(err.error.message);
        return throwError(err);
      })
    );
  }
  public crearUsuario(usuario) {
    const url = this.URL_SERVICIOS + '/registrar';
    console.log(url);
    console.log(usuario);
    return this._http.post(url, usuario).pipe(
      map((res: any) => {
        return res;
      }),
      catchError((err) => {
        this._alertsComponent.alertaInformativa(err.message);
        return throwError(err);
      })
    );
  }
  public actualizarUsuario(usuario: Usuario) {}
  public cambiarImagen(archivo: File, id: string) {
    // this._subirArchivoService
    //   .subirArchivo(archivo, 'usuarios', id)
    //   .then((resp: any) => {
    //     this.usuario.img_cover = resp.usuario.img;
    //     Swal.fire('Imagen Actualizada', this.usuario.nombre, 'success');
    //     this.guardarStorage(id, this.token, this.usuario, this.menu); // aqui no existe
    //   })
    //   .catch((resp) => {
    //     console.log(resp);
    //   });
  }
  public cargarUsuarios(desde: number = 0) {}
  public buscarUsuarios(termino: string) {}
}
