import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, filter, switchMap, delay } from 'rxjs/operators';
import { ok } from 'assert';
@Injectable({
  providedIn: 'root',
})
export class DataService {
  aaa: Observable<any>;
  constructor(private http: HttpClient) {}

  getUsers() {
    // return this.http.get('https://jsonplaceholder.typicode.com/users');
    return this.http.get('/assets/data/user.json');
  }

  getMenuOpts() {
    return this.http.get<any[]>('/assets/data/menu.json');
  }
  getOrders() {
    return this.http.get<any>('/assets/data/orders.json');
  }
  getOrderAnalytic() {
    return this.http.get<any>('/assets/data/order-analytic.json');
  }

  getOrdersDriver() {
    return this.http.get<any>('/assets/data/orders-driver.json');
  }
  getOrdersByDriver(id: any): Observable<any> {
    return this.http.get<any>('/assets/data/orders-driver.json').pipe(
      map((response: any) => {
        response = response.filter((x) => x.orderId == id);
        return response[0];
      })
    );
  }
  getOrdersByID(id: Number): Observable<any> {
    return this.http.get<any>('/assets/data/orders.json').pipe(
      map((response: any) => {
        //response[0];
        response = null;
        let arrayOrder: Array<any> = [];
        arrayOrder.push(JSON.parse(localStorage.getItem('order-detail')));
        if (arrayOrder[0] != null) {
          if (
            arrayOrder[0].dicount === 'undefined' ||
            arrayOrder[0].dicount === undefined
          ) {
            response = arrayOrder[0];
          } else {
            response = arrayOrder;
          }
        }
        response = response.filter((x) => x.orderId == id);
        return response[0];
      })
    );
  }
  getInvoiceByID(id: Number): Observable<any> {
    return this.http.get<any>('/assets/data/orders.json').pipe(
      map((response: any) => {
        return response[0];
      })
    );
  }
  getAlbumes() {
    return this.http.get<any[]>('https://jsonplaceholder.typicode.com/albums');
  }
  getHeroes() {
    return this.http.get('/assets/data/superheroes.json').pipe(delay(2000));
  }

  getVenueDetails(id) {
    return this.http.get<any>('/assets/data/getVenueDetails.json');
  }
  getVenueCategories(id) {
    return this.http.get<any>('/assets/data/getVenueCategories.json');
  }
  getFoods(id) {
    return this.http.get<any>('/assets/data/getFoods.json');
  }
  checkAuth() {
    return this.http.get<any>('/assets/data/getFoods.json');
  }

  checkAuth2() {
    return this.http.get<any>('/assets/data/checkAuth.json');
  }
  getOffers() {
    return this.http.get<any>('/assets/data/cupon.json');
  }

  getMyAddress(id: Number) {
    return this.http.get<any>('/assets/data/getAddress.json');
  }
  getMyAddress2(id: Number) {
    return this.http.get<any>('/assets/data/getAddress.json');
  }
  deleteAddress(id: string, id2: number) {
    return this.aaa;
  }

  addNewAddress(uid, id, param) {
    return this.aaa;
  }
  updateAddress(uid, id, param) {
    return this.aaa;
  }
}
