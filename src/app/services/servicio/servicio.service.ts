import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs/internal/observable/throwError';
import { SubirArchivoService } from '../subir-archivo/subir-archivo.service';
import { AlertsComponent } from 'src/app/components/alerts/alerts.component';
import { environment } from 'src/environments/environment';
import { Ordenes } from '../../interfaces/IOrdenes.interface';
@Injectable({
  providedIn: 'root',
})
export class ServicioService {
  private readonly URL_SERVICIOS = environment.URL_ENDPOINT + '/servicio';
  constructor(
    private _http: HttpClient,
    private _subirArchivoService: SubirArchivoService,
    private _alertsComponent: AlertsComponent
  ) {}

  public crearOrden(usuario: Ordenes) {}
  public actualizarOrden(usuario: Ordenes) {}
  public getServicio() {
    const url = this.URL_SERVICIOS + '/listar';
    return this._http.get(`${url}`).pipe(
      map((res: any) => {
        return res;
      }),
      catchError((err) => {
        this._alertsComponent.alertaInformativa(err.message);
        return throwError(err);
      })
    );
  }

  public buscarOrdenes(termino: Ordenes) {}
}
