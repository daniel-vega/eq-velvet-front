import { Injectable } from '@angular/core';
import { RegistroScan } from '../interfaces/registro.model';
//import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class DataLocalService {
  guardados: RegistroScan[] = [];

  constructor(private navCtrl: NavController) {
    // cargar registros
    this.cargarStorage();
  }

  cargarStorage() {
    this.guardados = JSON.parse(localStorage.getItem('registros')) || [];
  }

  guardarRegistro(format: string, text: string) {
    this.cargarStorage();

    const nuevoRegistro = new RegistroScan(format, text);
    this.guardados.unshift(nuevoRegistro);

    console.log(this.guardados);
    localStorage.setItem('registros', JSON.stringify(this.guardados));

    this.abrirRegistro(nuevoRegistro);
  }

  abrirRegistro(registro: RegistroScan) {
    this.navCtrl.navigateForward('/tabs/tab2');
    switch (registro.type) {
      case 'geo':
        this.navCtrl.navigateForward(`/tabs/tab2/mapa/${registro.text}`);
        break;
    }
  }
}
