import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-slides',
  templateUrl: './slides.component.html',
  styleUrls: ['./slides.component.scss'],
})
export class SlidesComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}
  slideOpts = {
    initialSlide: 0,
    speed: 400,
  };

  slides = [
    {
      img: 'assets/img/mujer.svg',
      titulo: 'Lava la ropa de tu mascota <br> cuando mas lo necesites',
    },
    {
      img: 'assets/img/pastel.svg',
      titulo: 'Facil y rapido<br> en solo 3 minutos',
    },
    {
      img: 'assets/img/dinero.svg',
      titulo: 'Seguimiento  <br> en tiempo real',
    },
  ];
  navigateToLoginPage() {
    this.router.navigate(['login']);
  }
}
