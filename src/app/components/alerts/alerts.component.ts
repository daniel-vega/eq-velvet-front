import { AlertController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
@Injectable({
  providedIn: 'root',
})
export class AlertsComponent {
  constructor(
    private alertController: AlertController,
    private toastController: ToastController
  ) {}

  async confirmLeave(titulo: string, mensaje: string): Promise<Boolean> {
    let resolveLeaving;
    const canLeave = new Promise<Boolean>(
      (resolve) => (resolveLeaving = resolve)
    );
    const alert = this.alertController.create({
      header: titulo,
      message: mensaje,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => resolveLeaving(false),
        },
        {
          text: 'Aceptar',
          handler: () => resolveLeaving(true),
        },
      ],
    });
    (await alert).present();
    return canLeave;
  }
  async presentToast(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 2000,
      color: 'primary',
    });
    toast.present();
  }
  async showToast(msg, colors, positon) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      color: colors,
      position: positon,
    });
    toast.present();
  }
  async alertaInformativa(message: string) {
    const alert = await this.alertController.create({
      message,
      buttons: ['OK'],
    });

    await alert.present();
  }
}
