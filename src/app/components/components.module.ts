import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SlidesComponent } from './slides/slides.component';
import { LogoComponent } from './logo/logo.component';
import { StartComponent } from './start/start.component';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { AvatarSelectorComponent } from './avatar-selector/avatar-selector.component';
import { MenuComponent } from './menu/menu.component';
import { PopoverComponent } from './popover/popover.component';

@NgModule({
  declarations: [
    SlidesComponent,
    LogoComponent,
    StartComponent,
    AvatarSelectorComponent,
    MenuComponent,
    PopoverComponent,
  ],
  exports: [
    SlidesComponent,
    LogoComponent,
    StartComponent,
    AvatarSelectorComponent,
    MenuComponent,
  ],
  imports: [CommonModule, IonicModule, RouterModule],
})
export class ComponentsModule {}
