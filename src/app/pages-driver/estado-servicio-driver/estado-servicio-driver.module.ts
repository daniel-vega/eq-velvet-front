import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EstadoServicioDriverPageRoutingModule } from './estado-servicio-driver-routing.module';

import { EstadoServicioDriverPage } from './estado-servicio-driver.page';
import { MatStepperModule } from '@angular/material/stepper';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EstadoServicioDriverPageRoutingModule,
    MatStepperModule,
    MatIconModule,
  ],
  declarations: [EstadoServicioDriverPage],
})
export class EstadoServicioDriverPageModule {}
