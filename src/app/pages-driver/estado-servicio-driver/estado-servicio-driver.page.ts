import { Component, OnInit } from '@angular/core';

import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-estado-servicio-driver',
  templateUrl: './estado-servicio-driver.page.html',
  styleUrls: ['./estado-servicio-driver.page.scss'],
})
export class EstadoServicioDriverPage implements OnInit {
  loaded: boolean;
  processing: boolean;
  currentStep: number;
  constructor(private router: Router) {}

  ngOnInit() {
    this.processing = true;
    setTimeout(() => {
      this.currentStep = 1;
      this.processing = false;
    }, 1000);
  }
  btnVerDetalle() {
    const navData: NavigationExtras = {
      queryParams: {
        id: 'vjCmW7p3RH',
      },
    };
    this.router.navigate(['home-driver/services-detail'], navData);
  }
}
