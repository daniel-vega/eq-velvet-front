import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EstadoServicioDriverPage } from './estado-servicio-driver.page';

const routes: Routes = [
  {
    path: '',
    component: EstadoServicioDriverPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EstadoServicioDriverPageRoutingModule {}
