import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChooseServiceScanPage } from './choose-service-scan.page';

const routes: Routes = [
  {
    path: '',
    component: ChooseServiceScanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChooseServiceScanPageRoutingModule {}
