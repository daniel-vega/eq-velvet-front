import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChooseServiceScanPageRoutingModule } from './choose-service-scan-routing.module';

import { ChooseServiceScanPage } from './choose-service-scan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChooseServiceScanPageRoutingModule
  ],
  declarations: [ChooseServiceScanPage]
})
export class ChooseServiceScanPageModule {}
