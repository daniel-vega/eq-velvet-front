import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { UtilService } from 'src/app/services/util.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-choose-service-scan',
  templateUrl: './choose-service-scan.page.html',
  styleUrls: ['./choose-service-scan.page.scss'],
})
export class ChooseServiceScanPage implements OnInit {
  seg_id = 1;
  orders: any[] = [];
  oldOrders: any;
  dummy = Array(50);
  constructor(
    private router: Router,
    private util: UtilService,
    private data: DataService
  ) {
    this.getOrders();
  }

  ngOnInit() {}

  onClick(val) {
    this.seg_id = val;
  }

  getOrders() {
    this.data.getOrdersDriver().subscribe(
      (data) => {
        this.dummy = [];
        console.log(data);
        if (data) {
          this.orders = [];
          this.oldOrders = [];
          data.forEach((element) => {
            element.order = JSON.parse(element.order);
            if (element.paid != 'after') {
              this.oldOrders.push(element);
            } else {
              this.orders.push(element);
            }
          });
        }
      },
      (err) => {
        this.dummy = [];
        console.log('eror', err);
      }
    );
  }

  goToOrderDetail(orderId) {
    const navData: NavigationExtras = {
      queryParams: {
        id: orderId,
        page: '0',
      },
    };
    this.router.navigate(['home-driver/update-service-status'], navData);
  }
  getProfilePic(item) {
    return item && item.cover ? item.cover : 'assets/img-compartidas/user.jpg';
  }
}
