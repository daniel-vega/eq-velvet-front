import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SettingsDriverPageRoutingModule } from './settings-driver-routing.module';

import { SettingsDriverPage } from './settings-driver.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SettingsDriverPageRoutingModule
  ],
  declarations: [SettingsDriverPage]
})
export class SettingsDriverPageModule {}
