import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { TipoUsuario } from 'src/app/interfaces/enums';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';

@Component({
  selector: 'app-settings-driver',
  templateUrl: './settings-driver.page.html',
  styleUrls: ['./settings-driver.page.scss'],
})
export class SettingsDriverPage implements OnInit {
  photo: any = 'assets/img-compartidas/user.jpg';
  _userRole: TipoUsuario;
  _admin: boolean;
  constructor(private router: Router, public _Usuario: UsuarioService) {}

  ngOnInit() {}

  ngDoCheck() {
    this._admin =
      this._Usuario.usuario.tipo_usuario == TipoUsuario.Administrador
        ? true
        : false;
  }
  goToEditProfile() {
    this.router.navigate(['/edit-profile']);
  }

  goToadminDirecciones() {
    const navData: NavigationExtras = {
      queryParams: {
        from: 'accont',
      },
    };
    this.router.navigate(['choose-address'], navData);
  }
  goToAdminRepartidores() {
    this.router.navigate(['choose-driver']);
  }
  goToEstadisticas() {
    this.router.navigate(['home-driver/analytics']);
  }
  logout() {
    this._Usuario.logout();
    this.router.navigate(['login']);
  }
}
