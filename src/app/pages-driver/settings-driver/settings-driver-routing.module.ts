import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingsDriverPage } from './settings-driver.page';

const routes: Routes = [
  {
    path: '',
    component: SettingsDriverPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingsDriverPageRoutingModule {}
