import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateServiceStatusPage } from './update-service-status.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateServiceStatusPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateServiceStatusPageRoutingModule {}
