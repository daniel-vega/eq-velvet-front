import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../services/data.service';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { UtilService } from 'src/app/services/util.service';
import { NavController } from '@ionic/angular';
import Swal from 'sweetalert2';
import { MatStepper } from '@angular/material/stepper';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { AlertsComponent } from '../../components/alerts/alerts.component';

@Component({
  selector: 'app-update-service-status',
  templateUrl: './update-service-status.page.html',
  styleUrls: ['./update-service-status.page.scss'],
})
export class UpdateServiceStatusPage implements OnInit {
  @ViewChild('stepper') stepper: MatStepper;
  tab_id;
  id: any;
  grandTotal: any;
  orders: any[] = [];
  serviceTax: any;
  status: any;
  time: any;
  total: any;
  uid: any;
  address: any;
  restName: any;
  deliveryAddress: any;
  username: any;
  useremail: any;
  userphone: any;
  usercover: any;
  payment: any;
  myname: any;
  token: any;
  changeStatusOrder: any;
  loaded: boolean;
  currentStep: number;
  constructor(
    private route: ActivatedRoute,
    private api: DataService,
    private router: Router,
    private util: UtilService,
    private navCtrl: NavController,
    private barcodeScanner: BarcodeScanner,
    private _Alerts: AlertsComponent
  ) {
    this.loaded = false;
  }
  page: number = 0;
  ngOnInit() {
    // this.route.queryParams.subscribe((data) => {
    //   console.log(data);
    //   this.tab_id = data.id;
    //   this.id = data.id;
    //   this.page = data.page;

    // });

    this.getOrder('vjCmW7p3RH');
  }

  processing: boolean;
  ngAfterViewInit() {
    this.processing = true;
    setTimeout(() => {
      this.stepperNext(2);
    }, 1000);
  }
  stepperNext(next) {
    let cont: number = 1;
    while (cont < next) {
      this.stepper.selected.completed = true;
      this.stepper.selected.editable = false;
      this.stepper.next();
      cont++;
    }
  }
  getOrder(id: string) {
    // this.util.show();
    this.api.getOrdersByDriver(id).subscribe(
      (data) => {
        // this.util.hide();
        this.loaded = true;
        console.log(data);
        if (data) {
          this.grandTotal = data.grandTotal;
          this.orders = JSON.parse(data.order);
          this.serviceTax = data.serviceTax;
          this.status = data.status;
          this.time = data.time;
          this.total = data.total;
          this.address = data.vid.address;
          this.restName = data.vid.name;
          this.deliveryAddress = data.address.address;
          this.username = data.uid.fullname;
          this.useremail = data.uid.email;
          this.userphone = data.uid.phone;
          this.usercover =
            data.uid && data.uid.cover
              ? data.uid.cover
              : 'assets/img-compartidas/user.jpg';
          this.payment = data.paid;
          this.myname = 'data.dId.fullname';
          this.token = 'data.uid.fcm_token';
          console.log('this', this.orders);
        }
      },
      (error) => {
        console.log('error in orders', error);
        // this.util.hide();
        this.loaded = true;
        this.util.errorToast(this.util.translate('Something went wrong'));
      }
    );
  }
  goToTracker() {
    const navData: NavigationExtras = {
      queryParams: {
        id: this.id,
      },
    };
    this.router.navigate(['home-driver/tracker'], navData);
  }
  call() {
    window.open('https://api.whatsapp.com/send?phone=91' + this.userphone);
  }
  mail() {
    window.open('mailto:' + this.useremail);
  }

  back() {
    this.util.publishNewAddress('hello');
    this.navCtrl.back();
  }

  btniniciarServicio() {
    this._Alerts
      .confirmLeave(
        '¿Estas seguro?',
        '¿Deseas actualizar el estatus del servicio?'
      )
      .then((res) => {
        if (res) {
          console.log('ok');
          this.router.navigate(['home-driver/tab1']);
          this._Alerts.presentToast('Se ha actualizado el estatus');
        }
      });
  }

  brCode() {
    this.barcodeScanner
      .scan()
      .then((barcodeData) => {
        if (!barcodeData.cancelled) {
          // this.dataLocal.guardarRegistro(barcodeData.format, barcodeData.text);
          //this.router.navigate(['home-driver/choose-service-scan']);
        }
      })
      .catch((err) => {
        this._Alerts.presentToast('Funcion disponible solo en movil');
        //      console.log('Error', err);
        //     // this.dataLocal.guardarRegistro( 'QRCode', 'https://fernando-herrera.com' );
        //     this.dataLocal.guardarRegistro( 'QRCode', 'geo:40.73151796986687,-74.06087294062502' );
        //this.router.navigate(['home-driver/choose-service-scan']);
      });
  }
}
