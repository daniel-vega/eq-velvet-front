import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateServiceStatusPageRoutingModule } from './update-service-status-routing.module';

import { UpdateServiceStatusPage } from './update-service-status.page';
import { MatStepperModule } from '@angular/material/stepper';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdateServiceStatusPageRoutingModule,
    MatStepperModule,
    MatIconModule,
  ],
  declarations: [UpdateServiceStatusPage],
})
export class UpdateServiceStatusPageModule {}
