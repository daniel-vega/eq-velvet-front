import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { UtilService } from 'src/app/services/util.service';
import { NavController } from '@ionic/angular';
import Swal from 'sweetalert2';
import { AlertsComponent } from '../../components/alerts/alerts.component';
import {
  LaunchNavigator,
  LaunchNavigatorOptions,
} from '@ionic-native/launch-navigator/ngx';

@Component({
  selector: 'app-services-detail',
  templateUrl: './services-detail.page.html',
  styleUrls: ['./services-detail.page.scss'],
})
export class ServicesDetailPage implements OnInit {
  tab_id;
  id: any;
  grandTotal: any;
  orders: any[] = [];
  serviceTax: any;
  status: any;
  time: any;
  total: any;
  uid: any;
  address: any;
  restName: any;
  deliveryAddress: any;
  username: any;
  useremail: any;
  userphone: any;
  usercover: any;
  payment: any;
  myname: any;
  token: any;
  changeStatusOrder: any;
  loaded: boolean;
  constructor(
    private route: ActivatedRoute,
    private api: DataService,
    private router: Router,
    private util: UtilService,
    private navCtrl: NavController,
    private _Alerts: AlertsComponent,
    private launchNavigator: LaunchNavigator
  ) {
    this.loaded = false;
  }
  page: number = 0;
  ngOnInit() {
    this.route.queryParams.subscribe((data) => {
      console.log(data);
      this.tab_id = data.id;
      this.id = data.id;
      this.page = data.page;
      this.getOrder();
    });
  }

  getOrder() {
    // this.util.show();
    this.api.getOrdersByDriver(this.id).subscribe(
      (data) => {
        // this.util.hide();
        this.loaded = true;
        console.log(data);
        if (data) {
          this.grandTotal = data.grandTotal;
          this.orders = JSON.parse(data.order);
          this.serviceTax = data.serviceTax;
          this.status = data.status;
          this.time = data.time;
          this.total = data.total;
          this.address = data.vid.address;
          this.restName = data.vid.name;
          this.deliveryAddress = data.address.address;
          this.username = data.uid.fullname;
          this.useremail = data.uid.email;
          this.userphone = data.uid.phone;
          this.usercover =
            data.uid && data.uid.cover
              ? data.uid.cover
              : 'assets/img-compartidas/user.jpg';
          this.payment = data.paid;
          this.myname = 'data.dId.fullname';
          this.token = 'data.uid.fcm_token';
          console.log('this', this.orders);
        }
      },
      (error) => {
        console.log('error in orders', error);
        // this.util.hide();
        this.loaded = true;
        this.util.errorToast(this.util.translate('Something went wrong'));
      }
    );
    // .catch((error) => {
    //   console.log('error in order', error);
    //   // this.util.hide();
    //   this.loaded = true;
    //   this.util.errorToast(this.util.translate('Something went wrong'));
    // });
  }
  // changeStatus(value) {
  //   this.util.show();
  //   this.api
  //     .updateOrderStatus(this.id, value)
  //     .then((data) => {
  //       console.log('data', data);
  //       const msg =
  //         this.util.translate('Your Order is ') +
  //         value +
  //         this.util.translate(' By ') +
  //         this.restName;
  //       if (value === 'delivered' || value === 'cancel') {
  //         const parm = {
  //           current: 'active',
  //         };
  //         this.api
  //           .updateProfile(localStorage.getItem('uid'), parm)
  //           .then((data) => {
  //             console.log('driver status cahcnage----->', data);
  //           })
  //           .catch((error) => {
  //             console.log(error);
  //           });
  //       }
  //       this.api.sendNotification(msg, 'Order ' + value, this.token).subscribe(
  //         (data) => {
  //           console.log(data);
  //           this.util.hide();
  //         },
  //         (error) => {
  //           this.util.hide();
  //           console.log('err', error);
  //         }
  //       );
  //       this.util.publishNewAddress('hello');
  //       Swal.fire({
  //         title: this.util.translate('success'),
  //         text: this.util.translate('Order status changed to ') + value,
  //         icon: 'success',
  //         timer: 2000,
  //         backdrop: false,
  //         background: 'white',
  //       });
  //       this.navCtrl.navigateRoot(['/tabs/tab1']);
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //       this.util.hide();
  //       this.navCtrl.navigateRoot(['/tabs/tab1']);
  //       this.util.errorToast(this.util.translate('Something went wrong'));
  //     });
  // }

  // changeOrderStatus() {
  //   console.log('order status', this.changeStatusOrder);
  //   if (this.changeStatusOrder) {
  //     this.changeStatus(this.changeStatusOrder);
  //   }
  // }

  goToTracker() {
    const navData: NavigationExtras = {
      queryParams: {
        id: this.id,
      },
    };
    this.router.navigate(['home-driver/tracker'], navData);
  }

  goToGoogleMaps() {
    let options: LaunchNavigatorOptions = {
      app: this.launchNavigator.APP.GOOGLE_MAPS,
    };

    this.launchNavigator
      .navigate(
        'Río Orinoco 519, Villas de Casa Blanca 1er Sector, 66475 San Nicolás de los Garza, N.L., México',
        options
      )
      .then(
        (success) => console.log('Launched navigator'),
        (error) => console.log('Error launching navigator', error)
      );
  }
  call() {
    window.open('https://api.whatsapp.com/send?phone=91' + this.userphone);
  }
  mail() {
    window.open('mailto:' + this.useremail);
  }

  back() {
    this.util.publishNewAddress('hello');
    this.navCtrl.back();
  }
  // picked() {
  //   this.util.show();
  //   this.api
  //     .updateOrderStatus(this.id, 'ongoing')
  //     .then(
  //       (data) => {
  //         console.log(data);
  //         this.util.hide();
  //         const msg =
  //           this.myname + this.util.translate(' Picked up your order');
  //         this.api
  //           .sendNotification(
  //             msg,
  //             this.util.translate('Order Picked'),
  //             this.token
  //           )
  //           .subscribe((data) => {
  //             console.log(data);
  //           });
  //         this.navCtrl.back();
  //         this.util.publishNewAddress('hello');
  //         Swal.fire({
  //           title: 'success',
  //           text: this.util.translate('Order status changed to ') + 'ongoing',
  //           icon: 'success',
  //           timer: 2000,
  //           backdrop: false,
  //           background: 'white',
  //         });
  //         this.navCtrl.back();
  //       },
  //       (error) => {
  //         this.util.hide();
  //         console.log('error', error);
  //       }
  //     )
  //     .catch((error) => {
  //       console.log(error);
  //       this.util.hide();
  //     });
  // }

  getCurrency() {
    return this.util.getCurrecySymbol();
  }

  // delivered() {
  //   this.util.show();
  //   this.api
  //     .updateOrderStatus(this.id, 'delivered')
  //     .then(
  //       (data) => {
  //         console.log(data);
  //         this.util.hide();
  //         const msg =
  //           this.myname + this.util.translate(' Delivered your order');
  //         const parm = {
  //           current: 'active',
  //         };
  //         this.api
  //           .updateProfile(localStorage.getItem('uid'), parm)
  //           .then((data) => {
  //             console.log('driver status cahcnage----->', data);
  //           })
  //           .catch((error) => {
  //             console.log(error);
  //           });
  //         this.api
  //           .sendNotification(
  //             msg,
  //             this.util.translate('Order delivered'),
  //             this.token
  //           )
  //           .subscribe((data) => {
  //             console.log(data);
  //           });
  //         this.navCtrl.back();
  //       },
  //       (error) => {
  //         this.util.hide();
  //         console.log('error', error);
  //         this.util.errorToast(this.util.translate('Something went wrong'));
  //       }
  //     )
  //     .catch((error) => {
  //       console.log(error);
  //       this.util.hide();
  //       this.util.errorToast(this.util.translate('Something went wrong'));
  //     });
  // }

  btniniciarServicio() {
    this._Alerts
      .confirmLeave('¿Estas seguro?', 'Se iniciara la recolección')
      .then((res) => {
        if (res) {
          console.log('ok');
          this.router.navigate(['home-driver/tab1']);
          this._Alerts.presentToast('Se ha iniciado una recolección');
        }
      });
  }
}
