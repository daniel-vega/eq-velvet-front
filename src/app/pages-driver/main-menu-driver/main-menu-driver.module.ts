import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MainMenuDriverPageRoutingModule } from './main-menu-driver-routing.module';

import { MainMenuDriverPage } from './main-menu-driver.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MainMenuDriverPageRoutingModule,
  ],
  declarations: [MainMenuDriverPage],
})
export class MainMenuDriverPageModule {}
