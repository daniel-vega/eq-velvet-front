import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TipoUsuario } from 'src/app/interfaces/enums';
import { UsuarioService } from '../../services/usuario/usuario.service';

@Component({
  selector: 'app-main-menu-driver',
  templateUrl: './main-menu-driver.page.html',
  styleUrls: ['./main-menu-driver.page.scss'],
})
export class MainMenuDriverPage implements OnInit {
  _userRole: TipoUsuario;
  _admin: boolean;

  constructor(private router: Router, public _Usuario: UsuarioService) {}

  ngOnInit() {
    this._admin =
      this._Usuario.usuario.tipo_usuario == TipoUsuario.Administrador
        ? true
        : false;
  }

  ngDoCheck() {
    // this._user = localStorage.getItem('_user');
    // this._admin = this._user == 'admin' ? true : false;
  }
  navigateToOrders() {
    this.router.navigate(['home-driver/all-services']);
  }
  navigateToInvoice() {
    this.router.navigate(['home-driver/invoice-driver']);
  }
  navigateToAgendaServicio() {
    this.router.navigate(['home-driver/choose-services']);
  }
}
