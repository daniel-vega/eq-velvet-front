import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditProfileDriverPageRoutingModule } from './edit-profile-driver-routing.module';

import { EditProfileDriverPage } from './edit-profile-driver.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditProfileDriverPageRoutingModule
  ],
  declarations: [EditProfileDriverPage]
})
export class EditProfileDriverPageModule {}
