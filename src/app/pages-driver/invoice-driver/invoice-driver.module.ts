import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InvoiceDriverPageRoutingModule } from './invoice-driver-routing.module';

import { InvoiceDriverPage } from './invoice-driver.page';
import { PipesModule } from '../../pipe/pipes.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InvoiceDriverPageRoutingModule,
    PipesModule,
  ],
  declarations: [InvoiceDriverPage],
})
export class InvoiceDriverPageModule {}
