import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InvoiceDriverPage } from './invoice-driver.page';

const routes: Routes = [
  {
    path: '',
    component: InvoiceDriverPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InvoiceDriverPageRoutingModule {}
