import { Component, OnInit, ViewChild } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { Observable } from 'rxjs';
import { IonList, IonSegment } from '@ionic/angular';

@Component({
  selector: 'app-invoice-driver',
  templateUrl: './invoice-driver.page.html',
  styleUrls: ['./invoice-driver.page.scss'],
})
export class InvoiceDriverPage implements OnInit {
  @ViewChild(IonSegment, { static: true }) segment: IonSegment;
  @ViewChild('lista', { static: false }) lista: IonList;
  superHeroes: Observable<any>;
  publisher = '';
  http: any;
  usuarios: Observable<any>;

  constructor(private dataService: DataService, private router: Router) {}

  ngOnInit() {
    this.segment.value = 'todos';
    this.superHeroes = this.dataService.getHeroes();
    this.usuarios = this.dataService.getUsers();
  }

  segmentChanged(event) {
    const valorSegmento = event.detail.value;

    if (valorSegmento === 'todos') {
      this.publisher = '';
      return;
    }

    this.publisher = 'caro';

    console.log(valorSegmento);
  }

  favorite(user) {
    console.log('favorite', user);
    this.lista.closeSlidingItems();
  }
  share(user) {
    console.log('share', user);
    this.lista.closeSlidingItems();
  }
  borrar(user) {
    console.log('borrar', user);
    this.lista.closeSlidingItems();
  }
  invoiceDetail() {
    const navData: NavigationExtras = {
      queryParams: {
        id: 'dGoztfCau2',
      },
    };

    this.router.navigate(['invoice-detail'], navData);
  }
}
