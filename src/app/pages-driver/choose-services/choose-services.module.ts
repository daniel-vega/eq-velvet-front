import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChooseServicesPageRoutingModule } from './choose-services-routing.module';

import { ChooseServicesPage } from './choose-services.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChooseServicesPageRoutingModule
  ],
  declarations: [ChooseServicesPage]
})
export class ChooseServicesPageModule {}
