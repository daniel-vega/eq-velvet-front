import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChooseServicesPage } from './choose-services.page';

const routes: Routes = [
  {
    path: '',
    component: ChooseServicesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChooseServicesPageRoutingModule {}
