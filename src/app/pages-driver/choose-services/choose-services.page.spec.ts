import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChooseServicesPage } from './choose-services.page';

describe('ChooseServicesPage', () => {
  let component: ChooseServicesPage;
  let fixture: ComponentFixture<ChooseServicesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseServicesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChooseServicesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
