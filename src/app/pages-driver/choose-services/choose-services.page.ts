import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-choose-services',
  templateUrl: './choose-services.page.html',
  styleUrls: ['./choose-services.page.scss'],
})
export class ChooseServicesPage implements OnInit {
  seg_id = 1;
  orders: any[] = [];
  oldOrders: any;
  dummy = Array(50);
  constructor(
    private router: Router,
    private util: UtilService,
    private data: DataService
  ) {
    // this.getOrders();
    // if (localStorage.getItem('uid')) {
    //   this.adb
    //     .collection('orders', (ref) =>
    //       ref.where('driverId', '==', localStorage.getItem('uid'))
    //     )
    //     .snapshotChanges()
    //     .subscribe(
    //       (data: any) => {
    //         console.log('paylaoddddd----->>>>', data);
    //         if (data) {
    this.getOrders();
    //         }
    //       },
    //       (error) => {
    //         console.log(error);
    //       }
    //     );
    // }
  }

  ngOnInit() {}

  onClick(val) {
    this.seg_id = val;
  }

  getOrders() {
    this.data.getOrdersDriver().subscribe(
      (data) => {
        this.dummy = [];
        console.log(data);
        if (data) {
          this.orders = [];
          this.oldOrders = [];
          data.forEach((element) => {
            element.order = JSON.parse(element.order);
            if (element.paid != 'after') {
              this.oldOrders.push(element);
            } else {
              this.orders.push(element);
            }
          });
        }
      },
      (err) => {
        this.dummy = [];
        console.log('eror', err);
      }
    );
  }
  //   this.orders = [];
  //   this.oldOrders = [];
  //   this.api
  //     .getMyOrders(localStorage.getItem('uid'))
  //     .then((data: any) => {
  //       this.dummy = [];
  //       console.log(data);
  //       if (data) {
  //         this.orders = [];
  //         this.oldOrders = [];
  //         data.forEach((element) => {
  //           element.order = JSON.parse(element.order);
  //           if (
  //             element.status === 'delivered' ||
  //             element.status === 'cancel' ||
  //             element.status === 'rejected'
  //           ) {
  //             this.oldOrders.push(element);
  //           } else {
  //             this.orders.push(element);
  //           }
  //         });
  //       }
  //     })
  //     .catch((error) => {
  //       this.dummy = [];
  //       console.log('eror', error);
  //     });
  // }
  goToOrderDetail(orderId) {
    const navData: NavigationExtras = {
      queryParams: {
        id: orderId,
        page: '0',
      },
    };
    this.router.navigate(['home-driver/services-detail'], navData);
  }
  getProfilePic(item) {
    return item && item.cover ? item.cover : 'assets/img-compartidas/user.jpg';
  }

  getCurrency() {
    return this.util.getCurrecySymbol();
  }
}
