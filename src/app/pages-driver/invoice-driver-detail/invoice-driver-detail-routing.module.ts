import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InvoiceDriverDetailPage } from './invoice-driver-detail.page';

const routes: Routes = [
  {
    path: '',
    component: InvoiceDriverDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InvoiceDriverDetailPageRoutingModule {}
