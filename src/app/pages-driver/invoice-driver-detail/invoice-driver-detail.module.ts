import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InvoiceDriverDetailPageRoutingModule } from './invoice-driver-detail-routing.module';

import { InvoiceDriverDetailPage } from './invoice-driver-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InvoiceDriverDetailPageRoutingModule
  ],
  declarations: [InvoiceDriverDetailPage]
})
export class InvoiceDriverDetailPageModule {}
