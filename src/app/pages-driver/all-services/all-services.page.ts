import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { UtilService } from 'src/app/services/util.service';
import { DataService } from '../../services/data.service';
import { UsuarioService } from '../../services/usuario/usuario.service';
import { OrdenService } from '../../services/orden/orden.service';
import { ordenModelPost } from 'src/app/interfaces/IOrdenes.interface';

@Component({
  selector: 'app-all-services',
  templateUrl: './all-services.page.html',
  styleUrls: ['./all-services.page.scss'],
})
export class AllServicesPage implements OnInit {
  seg_id = 1;
  ordenes: any[] = [];
  oldOrders: any[] = [];
  dummy = Array(50);
  constructor(
    private router: Router,
    private util: UtilService,
    private _ordenService: OrdenService,
    public _usuarioService: UsuarioService
  ) {
    {
      this.getOrders();
    }
  }

  ngOnInit() {}

  onClick(val) {
    this.seg_id = val;
  }

  getOrders() {
    this._ordenService.getOrdenesNoEntrgadas().subscribe(
      (data) => {
        if (data) {
          debugger;
          this.ordenes = <ordenModelPost[]>data;
          // let fecha1 = new Date(2020, , 1);
          // this.ordenes = this.ordenes.filter(
          //   (x) => new Date(x.fecha_orden) >= new Date(x.fecha_orden)
          // );
          this.dummy = [];
        }
      },
      (err) => {
        this.dummy = [];
        console.log('eror', err);
      }
    );
  }

  goToOrderDetail(orderId) {
    const navData: NavigationExtras = {
      queryParams: {
        id: orderId,
        page: '1',
      },
    };
    this.router.navigate(['home-driver/estado-servicio-d'], navData);
  }
  getProfilePic(item) {
    return item && item.cover ? item.cover : 'assets/img-compartidas/user.jpg';
  }

  getCurrency() {
    return this.util.getCurrecySymbol();
  }
}
