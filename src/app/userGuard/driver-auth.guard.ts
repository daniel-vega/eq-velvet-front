import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UsuarioService } from '../services/usuario/usuario.service';
import { TipoUsuario } from '../interfaces/enums';
@Injectable({
  providedIn: 'root',
})
export class AdminGuard implements CanActivate {
  constructor(public _usuariosService: UsuarioService, public route: Router) {}
  canActivate() {
    if (
      this._usuariosService.usuario.tipo_usuario === TipoUsuario.Driver ||
      this._usuariosService.usuario.tipo_usuario === TipoUsuario.Administrador
    ) {
      return true;
    } else {
      console.log('Bloqueado por el Admin Guard');
      this.route.navigate(['/login']);
      this._usuariosService.logout();
      return false;
    }
  }
}
