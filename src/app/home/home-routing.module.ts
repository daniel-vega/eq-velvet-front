import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: 'home',
    component: HomePage,
    children: [
      {
        path: 'tab1',
        loadChildren: () =>
          import('../pages/main-menu/main-menu.module').then(
            (m) => m.MainMenuPageModule
          ),
      },
      {
        path: 'tab2',
        loadChildren: () =>
          import('../pages/settings/settings.module').then(
            (m) => m.SettingsPageModule
          ),
      },
      {
        path: '',
        redirectTo: '/tabs/tab1',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: 'main-menu',
    loadChildren: () =>
      import('../pages/main-menu/main-menu.module').then(
        (m) => m.MainMenuPageModule
      ),
  },
  {
    path: 'services',
    loadChildren: () =>
      import('../pages/services/services.module').then(
        (m) => m.ServicesPageModule
      ),
  },
  {
    path: 'choose-address',
    loadChildren: () =>
      import('../pages/choose-address/choose-address.module').then(
        (m) => m.ChooseAddressPageModule
      ),
  },
  {
    path: 'add-new-address',
    loadChildren: () =>
      import('../pages/add-new-address/add-new-address.module').then(
        (m) => m.AddNewAddressPageModule
      ),
  },
  {
    path: 'edit-profile',
    loadChildren: () =>
      import('../pages/edit-profile/edit-profile.module').then(
        (m) => m.EditProfilePageModule
      ),
  },
  {
    path: 'choose-driver',
    loadChildren: () =>
      import('../pages/choose-driver/choose-driver.module').then(
        (m) => m.ChooseDriverPageModule
      ),
  },
  {
    path: 'orders',
    loadChildren: () =>
      import('../pages/orders/orders.module').then((m) => m.OrdersPageModule),
  },
  {
    path: 'orders-detail',
    loadChildren: () =>
      import('../pages/orders-detail/orders-detail.module').then(
        (m) => m.OrdersDetailPageModule
      ),
  },
  {
    path: 'agenda-servicio',
    loadChildren: () =>
      import('../pages/agenda-servicio/agenda-servicio.module').then(
        (m) => m.AgendaServicioPageModule
      ),
  },
  {
    path: 'agenda-servicio-detail',
    loadChildren: () =>
      import(
        '../pages/agenda-servicio-detail/agenda-servicio-detail.module'
      ).then((m) => m.AgendaServicioDetailPageModule),
  },
  {
    path: 'invoice',
    loadChildren: () =>
      import('../pages/invoice/invoice.module').then(
        (m) => m.InvoicePageModule
      ),
  },
  {
    path: 'invoice-detail',
    loadChildren: () =>
      import('../pages/invoice-detail/invoice-detail.module').then(
        (m) => m.InvoiceDetailPageModule
      ),
  },

  {
    path: 'variations',
    loadChildren: () =>
      import('../variations/variations.module').then(
        (m) => m.VariationsPageModule
      ),
  },
  {
    path: 'cart',
    loadChildren: () =>
      import('../pages/cart/cart.module').then((m) => m.CartPageModule),
  },
  {
    path: 'coupons',
    loadChildren: () =>
      import('../pages/coupons/coupons.module').then(
        (m) => m.CouponsPageModule
      ),
  },
  {
    path: 'payments',
    loadChildren: () =>
      import('../pages/payments/payments.module').then(
        (m) => m.PaymentsPageModule
      ),
  },
  {
    path: 'estado-servicio',
    loadChildren: () =>
      import('../pages/estado-servicio/estado-servicio.module').then(
        (m) => m.EstadoServicioPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule {}
