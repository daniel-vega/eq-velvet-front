import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeDriverPage } from './home-driver.page';
import { EditProfileDriverPageModule } from '../pages-driver/edit-profile-driver/edit-profile-driver.module';

const routes: Routes = [
  {
    path: '',
    component: HomeDriverPage,
    children: [
      {
        path: 'tab1',
        loadChildren: () =>
          import(
            '../pages-driver/main-menu-driver/main-menu-driver.module'
          ).then((m) => m.MainMenuDriverPageModule),
      },

      {
        path: 'tab3',
        loadChildren: () =>
          import('../pages-driver/settings-driver/settings-driver.module').then(
            (m) => m.SettingsDriverPageModule
          ),
      },

      {
        path: '',
        redirectTo: '/tabs/tab1',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: 'choose-services',
    loadChildren: () =>
      import('../pages-driver/choose-services/choose-services.module').then(
        (m) => m.ChooseServicesPageModule
      ),
  },
  {
    path: 'all-services',
    loadChildren: () =>
      import('../pages-driver/all-services/all-services.module').then(
        (m) => m.AllServicesPageModule
      ),
  },
  {
    path: 'invoice-driver',
    loadChildren: () =>
      import('../pages-driver/invoice-driver/invoice-driver.module').then(
        (m) => m.InvoiceDriverPageModule
      ),
  },
  {
    path: 'services-detail',
    loadChildren: () =>
      import('../pages-driver/services-detail/services-detail.module').then(
        (m) => m.ServicesDetailPageModule
      ),
  },
  {
    path: 'tracker',
    loadChildren: () =>
      import('../pages-driver/tracker/tracker.module').then(
        (m) => m.TrackerPageModule
      ),
  },

  {
    path: 'invoice-driver-detail',
    loadChildren: () =>
      import(
        '../pages-driver/invoice-driver-detail/invoice-driver-detail.module'
      ).then((m) => m.InvoiceDriverDetailPageModule),
  },
  {
    path: 'edit-profile-driver',
    loadChildren: () =>
      import(
        '../pages-driver/edit-profile-driver/edit-profile-driver.module'
      ).then((m) => m.EditProfileDriverPageModule),
  },
  {
    path: 'analytics',
    loadChildren: () =>
      import('../pages-driver/analytics/analytics.module').then(
        (m) => m.AnalyticsPageModule
      ),
  },
  {
    path: 'estado-servicio-d',
    loadChildren: () =>
      import(
        '../pages-driver/estado-servicio-driver/estado-servicio-driver.module'
      ).then((m) => m.EstadoServicioDriverPageModule),
  },
  {
    path: 'choose-service-scan',
    loadChildren: () =>
      import(
        '../pages-driver/choose-service-scan/choose-service-scan.module'
      ).then((m) => m.ChooseServiceScanPageModule),
  },
  {
    path: 'update-service-status',
    loadChildren: () =>
      import(
        '../pages-driver/update-service-status/update-service-status.module'
      ).then((m) => m.UpdateServiceStatusPageModule),
  },
  {
    path: 'edit-profile-driver',
    loadChildren: () =>
      import(
        '../pages-driver/edit-profile-driver/edit-profile-driver.module'
      ).then((m) => m.EditProfileDriverPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeDriverPageRoutingModule {}
