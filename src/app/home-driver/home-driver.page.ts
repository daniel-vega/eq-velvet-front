import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { DataLocalService } from '../services/data-local.service';
import { Router } from '@angular/router';
import { UsuarioService } from '../services/usuario/usuario.service';
import { TipoUsuario } from '../interfaces/enums';

@Component({
  selector: 'app-home-driver',
  templateUrl: './home-driver.page.html',
  styleUrls: ['./home-driver.page.scss'],
})
export class HomeDriverPage implements OnInit {
  _userRole: TipoUsuario;
  _admin: boolean;
  constructor(
    private barcodeScanner: BarcodeScanner,
    private dataLocal: DataLocalService,
    private router: Router,
    private _Usuario: UsuarioService
  ) {}

  ngOnInit() {}
  ngDoCheck() {
    this._userRole = this._Usuario.usuario.tipo_usuario;
    this._admin = this._userRole == TipoUsuario.Administrador ? true : false;
  }
  brCode() {
    this.barcodeScanner
      .scan()
      .then((barcodeData) => {
        if (!barcodeData.cancelled) {
          this.router.navigate(['home-driver/choose-service-scan']);
        }
      })
      .catch((err) => {
        this.router.navigate(['home-driver/choose-service-scan']);
      });
  }
}
