import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {
  constructor() {}

  ngOnInit() {}
  slideOpts = {
    initialSlide: 0,
    speed: 400,
  };

  slides = [
    {
      img: 'assets/img/mujer.svg',
      titulo: 'Lava la ropa de tu mascota <br> cuando mas lo necesites',
    },
    {
      img: 'assets/img/pastel.svg',
      titulo: 'Facil y rapido<br> en solo 3 minutos',
    },
    {
      img: 'assets/img/dinero.svg',
      titulo: 'Seguimiento  <br> en tiempo real',
    },
  ];
}
