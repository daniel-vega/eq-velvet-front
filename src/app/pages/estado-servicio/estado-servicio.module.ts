import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EstadoServicioPageRoutingModule } from './estado-servicio-routing.module';

import { EstadoServicioPage } from './estado-servicio.page';
import { MatStepperModule } from '@angular/material/stepper';
import { MatIconModule } from '@angular/material/icon';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EstadoServicioPageRoutingModule,
    MatStepperModule,
    MatIconModule,
  ],
  declarations: [EstadoServicioPage],
})
export class EstadoServicioPageModule {}
