import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router, ActivatedRoute } from '@angular/router';
import { MatStepperModule } from '@angular/material/stepper';
import { MatIconModule } from '@angular/material/icon';
import { UsuarioService } from '../../services/usuario/usuario.service';

@Component({
  selector: 'app-estado-servicio',
  templateUrl: './estado-servicio.page.html',
  styleUrls: ['./estado-servicio.page.scss'],
})
export class EstadoServicioPage implements OnInit {
  loaded: boolean;
  processing: boolean;
  currentStep: number;
  id: number;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public _usuarioService: UsuarioService
  ) {
    this.route.queryParams.subscribe((data) => {
      console.log('data=>', data);
      if (data.hasOwnProperty('id')) {
        this.id = data.id;
        //this.getOrder();
      }
    });
  }

  ngOnInit() {
    this.processing = true;
    setTimeout(() => {
      this.currentStep = 1;
      this.processing = false;
    }, 1000);
  }
  btnVerDetalle() {
    const navData: NavigationExtras = {
      queryParams: {
        id: this.id,
      },
    };
    this.router.navigate(['orders-detail'], navData);
  }
}
