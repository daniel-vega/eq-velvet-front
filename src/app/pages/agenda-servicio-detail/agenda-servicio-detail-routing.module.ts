import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgendaServicioDetailPage } from './agenda-servicio-detail.page';

const routes: Routes = [
  {
    path: '',
    component: AgendaServicioDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgendaServicioDetailPageRoutingModule {}
