import { Component, OnInit, ViewChild } from '@angular/core';
import { NavigationExtras } from '@angular/router';
import { UtilService } from 'src/app/services/util.service';
import { NavController } from '@ionic/angular';
import { ServicioService } from '../../services/servicio/servicio.service';
import { servicios } from '../../interfaces/IOrdenes.interface';

@Component({
  selector: 'app-agenda-servicio-detail',
  templateUrl: './agenda-servicio-detail.page.html',
  styleUrls: ['./agenda-servicio-detail.page.scss'],
})
export class AgendaServicioDetailPage implements OnInit {
  @ViewChild('content', { static: false }) private content: any;
  servicios: servicios[] = [];
  dummyFoods: any[] = [];
  categories: any[] = [];
  dummy = Array(50);
  totalItem: any = 0;
  totalPrice: any = 0;
  totalPriceString: any = 0;
  deliveryAddress: any = '';
  cart: any[] = [];
  constructor(
    private _apiServicio: ServicioService,
    private util: UtilService,
    private navCtrl: NavController
  ) {}

  ngOnInit() {
    this.getCategorias();
    this.getServicios();
  }
  // direccion de la sede club Hipico
  getAddress() {
    const address = JSON.parse(localStorage.getItem('deliveryAddress'));
    if (address && address.address) {
      this.deliveryAddress = address.address;
    }
    return this.deliveryAddress;
  }

  getCategorias() {
    this._apiServicio.getServicio().subscribe(
      (categoria) => {
        if (categoria) {
          this.categories = categoria;
        }
      },
      (error) => {
        console.log(error);
        this.dummy = [];
        this.util.errorToast(this.util.translate('Something went wrong'));
      }
    );
  }

  getServicios() {
    this._apiServicio.getServicio().subscribe(
      (servicio) => {
        console.log(servicio);
        if (servicio) {
          this.dummy = [];
          this.servicios = [];
          this.dummyFoods = [];
          this.servicios = <servicios[]>servicio;
          this.dummyFoods = servicio;

          if (!this.servicios.length || this.servicios.length === 0) {
            this.util.errorToast('No se encontraron servicios');
            this.navCtrl.back();
            return false;
          }
          this.changeStatus();
          //this.checkCart();
        }
      },
      (error) => {
        console.log(error);
        this.dummy = [];
        this.util.errorToast('Ocurrio un error');
      }
    );
  }

  // checkCart() {
  //   const userCart = localStorage.getItem('userCart');
  //   if (
  //     userCart &&
  //     userCart !== 'null' &&
  //     userCart !== undefined &&
  //     userCart !== 'undefined'
  //   ) {
  //     const cart = JSON.parse(userCart);
  //     console.log('carrt', cart);
  //     console.log(this.foodIds);
  //     cart.forEach((element) => {
  //       if (this.foodIds.includes(element.id)) {
  //         const index = this.foods.findIndex((x) => x.id === element.id);
  //         console.log('index---<', index);
  //         this.foods[index].quantiy = element.quantiy;
  //         this.foods[index].selectedItem = element.selectedItem;
  //       }
  //     });
  //     this.calculate();
  //   }
  // }
  back() {
    this.navCtrl.navigateForward(['home/tab1']);
  }

  getCusine(cusine) {
    return cusine.join('-');
  }
  add(index) {
    this.servicios[index].cantidad += 1;
    this.calculate(index);
  }

  calculate(index) {
    this.dummy = [];
    this.totalItem = 0;
    this.totalPrice = 0;
    let item = this.servicios.filter((x) => x.cantidad > 0);
    console.log(item);
    if (item.length > 0) {
      item.forEach((element) => {
        this.totalItem = this.totalItem + element.cantidad;
        this.totalPrice =
          this.totalPrice +
          parseFloat(element.precio.toString()) *
            parseInt(element.cantidad.toString());
      });
    } else {
      this.totalItem = 0;
    }
    this.totalPriceString = parseFloat(this.totalPrice).toFixed(2);
    if (this.totalItem === 0) {
      this.totalItem = 0;
      this.totalPrice = 0;
      this.totalPriceString;
    }
  }

  async setData() {
    if (this.totalPrice > 0) {
      await localStorage.setItem('cartService', JSON.stringify(this.servicios));
    }
  }
  async ionViewWillLeave() {
    await this.setData();
  }
  changeStatus() {
    this.servicios = this.dummyFoods.filter((x) => x.estatus === 'activo');
  }

  addQ(index) {
    this.servicios[index].cantidad = this.servicios[index].cantidad + 1;
    this.calculate(index);
  }

  removeQ(index) {
    if (this.servicios[index].cantidad !== 0) {
      if (this.servicios[index].cantidad >= 1) {
        this.servicios[index].cantidad = this.servicios[index].cantidad - 1;
      } else {
        this.servicios[index].cantidad = 0;
      }
      this.calculate(index);
    }
  }

  // async presentAlertConfirm() {
  //   // editado
  //   console.log('Confirm Okay');
  //   localStorage.removeItem('vid');
  //   this.dummy = Array(10);
  //   localStorage.removeItem('categories');
  //   localStorage.removeItem('dummyItem');
  //   localStorage.removeItem('foods');
  //   this.totalItem = 0;
  //   this.totalPrice = 0;
  //   this.getCategorias();
  //   this.getServicios();
  // }

  viewCart() {
    console.log('viewCart');
    const navData: NavigationExtras = {
      queryParams: {
        from: 'cart',
      },
    };
    this.navCtrl.navigateRoot(['choose-address'], navData);
  }
}
