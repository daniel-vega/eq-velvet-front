import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgendaServicioDetailPageRoutingModule } from './agenda-servicio-detail-routing.module';

import { AgendaServicioDetailPage } from './agenda-servicio-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgendaServicioDetailPageRoutingModule
  ],
  declarations: [AgendaServicioDetailPage]
})
export class AgendaServicioDetailPageModule {}
