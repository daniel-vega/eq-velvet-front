import { Component, OnInit } from '@angular/core';
import { NavController, ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  name: any = 'Daniel ';
  profilePic: any = 'assets/img-compartidas/user.jpg';
  phone: any = '6441451840';
  descriptions: any = 'El Mejor';
  handle: any = 'Vega Alvarado';
  check = true;
  _user: string = localStorage.getItem('_user');
  _admin: boolean;
  constructor(
    private navCtrl: NavController,
    private actionSheetController: ActionSheetController
  ) {}

  ngOnInit() {
    console.log(this._admin);
    this._admin = this._user == 'admin' ? true : false;
  }

  ngDoCheck() {
    this._user = localStorage.getItem('_user');
    this._admin = this._user == 'admin' ? true : false;
  }
  async cover() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Choose from',
      buttons: [
        {
          text: 'Camera',
          icon: 'camera',
          handler: () => {
            console.log('Delete clicked');
            // this.opemCamera('camera');
          },
        },
        {
          text: 'Gallery',
          icon: 'image',
          handler: () => {
            console.log('Share clicked');
            //this.opemCamera('gallery');
          },
        },
        {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          },
        },
      ],
    });
    await actionSheet.present();
  }

  update() {
    this.navCtrl.back();
  }
}
