import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { ordenModelPost } from 'src/app/interfaces/IOrdenes.interface';
import { OrdenService } from '../../services/orden/orden.service';
import { UsuarioService } from '../../services/usuario/usuario.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {
  seg_id = 1;
  ordenes: ordenModelPost[] = [];
  dummy = Array(50);
  orderView: any;
  constructor(
    public _router: Router,
    private _ordenService: OrdenService,
    public _usuarioServoce: UsuarioService
  ) {
    this.getOrdersNoEntregadas(this._usuarioServoce.usuario.id);
  }

  ngOnInit() {}

  onClick(val) {
    this.seg_id = val;
  }

  getOrdersNoEntregadas(idUsusuario: number) {
    this._ordenService.getOrdenesNoEntrgadasByID(idUsusuario).subscribe(
      (data) => {
        if (data) {
          debugger;
          this.ordenes = <ordenModelPost[]>data;
          this.dummy = [];
        }
      },
      (err) => {
        this.dummy = [];
        console.log('eror', err);
      }
    );
  }

  goToOrderDetail(orderId) {
    this.orderView = this.ordenes.filter((x) => x.id === orderId);
    localStorage.setItem('orderView', JSON.stringify(this.orderView));
    const navData: NavigationExtras = {
      queryParams: {
        id: orderId,
      },
    };
    this._router.navigate(['estado-servicio'], navData);
  }

  getProfilePic(item) {
    return item && item.cover ? item.cover : 'assets/img-compartidas/user.jpg';
  }

  navigateToAgenda() {
    this._router.navigate(['agenda-servicio-detail']);
  }
}
