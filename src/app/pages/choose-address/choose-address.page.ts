import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { ApisService } from 'src/app/services/apis.service';
import { UtilService } from 'src/app/services/util.service';
import { NavController, PopoverController } from '@ionic/angular';
import { PopoverComponent } from 'src/app/components/popover/popover.component';
import { DataService } from '../../services/data.service';
import { AlertsComponent } from '../../components/alerts/alerts.component';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-choose-address',
  templateUrl: './choose-address.page.html',
  styleUrls: ['./choose-address.page.scss'],
})
export class ChooseAddressPage implements OnInit {
  id: any;
  myaddress: Observable<any>;
  from: any;
  selectedAddress: any;
  dummy = Array(10);
  seg_id = 2;
  club = 'club';
  constructor(
    private router: Router,
    private api: DataService,
    private route: ActivatedRoute,
    private popoverController: PopoverController,
    private _Alerts: AlertsComponent
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe((data) => {
      console.log(data);
      if (data && data.from) {
        this.from = data.from;
      }
    });
  }

  getAddress() {
    this.myaddress = this.api.getMyAddress2(this.id);
    console.log(this.myaddress);
    // this.api.getMyAddress(this.id).subscribe(
    //   (data) => {
    //     console.log('my address', data);
    //     this.dummy = [];
    //     debugger;
    //     if (data && data.length) {
    //       this.myaddress = data;
    //       debugger;
    //     }
    //   },
    //   (error) => {
    //     console.log(error);
    //     this.dummy = [];
    //   }
    // );
    // .catch((error) => {
    //   console.log(error);
    //   this.dummy = [];
    // });
  }

  ionViewWillEnter() {
    this.api.checkAuth().subscribe((data: any) => {
      console.log(data);
      if (data) {
        this.id = data.uid;
        this.getAddress();
      }
    });
  }

  addNew() {
    this.router.navigate(['add-new-address']);
  }

  selectAddress() {
    if (this.from === 'cart') {
      this.api.getMyAddress(this.id).subscribe((data) => {
        data.filter((x) => x.id === this.selectedAddress);
        const item = data[0];
        console.log('item', item);
        const address = {
          address: item.house + ' ' + item.landmark + ' ' + item.address,
          lat: item.lat,
          lng: item.lng,
          id: item.id,
        };
        localStorage.setItem('deliveryAddress', JSON.stringify(address));
        this.router.navigate(['cart']);
      });
    }
  }

  async openMenu(item, events) {
    const popover = await this.popoverController.create({
      component: PopoverComponent,
      event: events,
      mode: 'ios',
    });
    popover.onDidDismiss().then((data) => {
      console.log(data.data);
      if (data && data.data) {
        if (data.data === 'edit') {
          const navData: NavigationExtras = {
            queryParams: {
              from: 'edit',
              data: JSON.stringify(item),
            },
          };
          this.router.navigate(['add-new-address'], navData);
        } else if (data.data === 'delete') {
          this._Alerts
            .confirmLeave('¿Estas seguro?', 'De eliminar esta dirección')
            .then((res) => {
              if (res) {
                this._Alerts.presentToast('Proceso aun en desarrollo.');
              }
            });
        }
      }
    });
    await popover.present();
  }

  onClick(val) {
    this.seg_id = val;
  }
}
