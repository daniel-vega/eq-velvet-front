import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginChooseAccountPageRoutingModule } from './login-choose-account-routing.module';

import { LoginChooseAccountPage } from './login-choose-account.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginChooseAccountPageRoutingModule
  ],
  declarations: [LoginChooseAccountPage]
})
export class LoginChooseAccountPageModule {}
