import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginChooseAccountPage } from './login-choose-account.page';

const routes: Routes = [
  {
    path: '',
    component: LoginChooseAccountPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginChooseAccountPageRoutingModule {}
