import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-login-choose-account',
  templateUrl: './login-choose-account.page.html',
  styleUrls: ['./login-choose-account.page.scss'],
})
export class LoginChooseAccountPage implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}
  registerClient() {
    this.navigate('cliente');
  }
  registerdriver() {
    this.navigate('driver');
  }

  navigate(tipo) {
    const navData: NavigationExtras = {
      queryParams: {
        tipo: tipo,
      },
    };
    this.router.navigate(['register'], navData);
  }
}
