import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { UtilService } from 'src/app/services/util.service';
import { NavController } from '@ionic/angular';
import {
  ordenDetalle,
  ordenModelPost,
  servicios,
} from 'src/app/interfaces/IOrdenes.interface';
import { AlertsComponent } from 'src/app/components/alerts/alerts.component';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { OrdenService } from 'src/app/services/orden/orden.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
  totalPrice: any = 0;
  totalItem: any = 0;
  deliveryAddress: any = '';
  coupon: any;
  dicount: any;
  totalPriceString: any;
  cart: servicios[] = [];
  servicios: servicios[] = [];
  priceEnvio: number = 100;
  ordenPost: ordenModelPost = <ordenModelPost>{};
  constructor(
    private router: Router,
    private util: UtilService,
    private navCtrl: NavController,
    private chMod: ChangeDetectorRef,
    private _Alerts: AlertsComponent,
    private _usuario: UsuarioService,
    private _orden: OrdenService
  ) {
    this.util.getCouponObservable().subscribe((data) => {
      if (data) {
        console.log(data);
        this.coupon = data;
        console.log('coupon', this.coupon);
        console.log(this.totalPrice);
        localStorage.setItem('coupon', JSON.stringify(data));

        this.calculate();
      }
    });
  }

  ngOnInit() {}
  ionViewWillEnter() {
    this.validate();
  }
  getAddress() {
    const add = JSON.parse(localStorage.getItem('deliveryAddress'));
    if (add && add.address) {
      this.deliveryAddress = add.address;
    }
    return this.deliveryAddress;
  }

  validate() {
    const cart = localStorage.getItem('cartService');
    try {
      if (
        cart &&
        cart !== 'null' &&
        cart !== undefined &&
        cart !== 'undefined'
      ) {
        this.cart = JSON.parse(localStorage.getItem('cartService'));
        console.log('cart', JSON.stringify(this.cart));
        this.calculate();
      } else {
        this.cart = [];
      }
    } catch (error) {
      console.log(error);
      this.cart = [];
    }

    this.chMod.detectChanges();
    return true;
  }

  placeOrder() {
    this._Alerts
      .confirmLeave('¿Estas seguro?', 'Se iniciará un nuevo servicio')
      .then((res) => {
        if (res) {
          console.log('ok');
          this.createOrder();
          this.navCtrl.navigateRoot(['home/tab1']);
          this._Alerts.presentToast('Tu servicio se ha creado correctamente');
        }
      });
  }

  async createOrder() {
    let ordenDetalles: ordenDetalle[] = <ordenDetalle[]>[];
    let item = this.cart.filter((x) => x.cantidad > 0);
    if (item.length > 0) {
      item.forEach((element) => {
        this.totalItem = this.totalItem + element.cantidad;
        this.totalPrice =
          this.totalPrice +
          parseFloat(element.precio.toString()) *
            parseInt(element.cantidad.toString());
        let detalle: ordenDetalle = <ordenDetalle>{
          id_servicio: element.id,
          cantidad: element.cantidad,
          subtotal: element.precio * element.cantidad,
        };
        ordenDetalles.push(detalle);
      });
    }

    this.ordenPost.gastos_envio = 100;
    this.ordenPost.descuento = 0;
    this.ordenPost.total = Number(this.totalPriceString); //this.totalPrice;
    this.ordenPost.iva = 16;
    this.ordenPost.fecha_orden = new Date();
    this.ordenPost.subtotal = this.totalPrice;
    this.ordenPost.estatus = 'En proceso';
    this.ordenPost.estatus_pago = 'Pendiente';
    this.ordenPost.id_direccion = 1;
    this.ordenPost.id_driver = 2;
    this.ordenPost.id_usuario = this._usuario.usuario.id;
    this.ordenPost.id_lavanderia = null;
    this.ordenPost.id_cupon = null;
    let OrdenPost = { orden: this.ordenPost, orden_detalle: ordenDetalles };
    debugger;
    this.enviarOrden(OrdenPost);
  }

  enviarOrden(ordenPost) {
    this._orden.crearOrden(ordenPost).subscribe((data) => {
      debugger;
    });
  }

  getCart() {
    this.navCtrl.navigateRoot(['tabs/tab1']);
  }
  addQ(index) {
    this.cart[index].cantidad = this.cart[index].cantidad + 1;
    this.calculate();
  }
  removeQ(index) {
    if (this.cart[index].cantidad !== 0) {
      this.cart[index].cantidad = this.cart[index].cantidad - 1;
    } else {
      this.cart[index].cantidad = 0;
    }
    localStorage.setItem('cartService', JSON.stringify(this.cart));
    this.calculate();
  }

  async calculate() {
    // this.dummy = [];
    this.totalItem = 0;
    this.totalPrice = 0;
    let item = this.cart.filter((x) => x.cantidad > 0);
    console.log(item);
    if (item.length > 0) {
      item.forEach((element) => {
        this.totalItem = this.totalItem + element.cantidad;
        this.totalPrice =
          this.totalPrice +
          parseFloat(element.precio.toString()) *
            parseInt(element.cantidad.toString());
      });
    } else {
      this.totalItem = 0;
    }
    this.totalPriceString = parseFloat(
      this.totalPrice + this.priceEnvio
    ).toFixed(2);
    if (this.totalItem === 0) {
      this.totalItem = 0;
      this.totalPrice = 0;
      this.totalPriceString = 0;
    }
  }

  getCurrency() {
    return this.util.getCurrecySymbol();
  }

  changeAddress() {
    const navData: NavigationExtras = {
      queryParams: {
        from: 'cart',
      },
    };
    this.router.navigate(['choose-address'], navData);
  }
  checkout() {
    const navData: NavigationExtras = {
      queryParams: {
        from: 'cart',
      },
    };
    this.router.navigate(['payments'], navData);
  }
  openCoupon() {
    const navData: NavigationExtras = {
      queryParams: {
        totalPrice: this.totalPrice,
      },
    };
    this.router.navigate(['coupons'], navData);
  }
}
