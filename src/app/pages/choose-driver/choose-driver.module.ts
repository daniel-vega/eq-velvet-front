import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChooseDriverPageRoutingModule } from './choose-driver-routing.module';

import { ChooseDriverPage } from './choose-driver.page';
import { PipesModule } from '../../pipe/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    ChooseDriverPageRoutingModule,
  ],
  declarations: [ChooseDriverPage],
})
export class ChooseDriverPageModule {}
