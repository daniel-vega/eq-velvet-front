import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { IonList } from '@ionic/angular';
import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-choose-driver',
  templateUrl: './choose-driver.page.html',
  styleUrls: ['./choose-driver.page.scss'],
})
export class ChooseDriverPage implements OnInit {
  @ViewChild('lista') lista: IonList;

  albumes: any[] = [];
  textoBuscar = '';

  constructor(private dataService: DataService, private router: Router) {}

  ngOnInit() {
    this.albumes = null;
    this.dataService.getAlbumes().subscribe((albumes) => {
      console.log(albumes);
      this.albumes = albumes;
    });
  }

  buscar(event) {
    // console.log(event);
    this.textoBuscar = event.detail.value;
  }
  navegateToProfile() {
    this.router.navigate(['home-driver/edit-profile-driver']);
  }
}
