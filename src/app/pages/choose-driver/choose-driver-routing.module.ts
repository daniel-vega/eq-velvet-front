import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChooseDriverPage } from './choose-driver.page';

const routes: Routes = [
  {
    path: '',
    component: ChooseDriverPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChooseDriverPageRoutingModule {}
