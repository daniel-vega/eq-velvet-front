import { NgForm } from '@angular/forms';
import { IonSlides, NavController } from '@ionic/angular';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { UtilService } from 'src/app/services/util.service';
import { UsuarioService } from '../../services/usuario/usuario.service';
import { TipoUsuario } from '../../interfaces/enums';
import { Usuario } from '../../interfaces/IUsuarios.interface';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  @ViewChild('slidePrincipal') slides: IonSlides;
  login = { email: 'prueba@cliente.com', password: '1234567' };
  submitted = false;
  isLogin: boolean = false;

  constructor(
    private _router: Router,
    private _navCtrl: NavController,
    private _util: UtilService,
    private _usuarioService: UsuarioService
  ) {}

  ngOnInit() {
    localStorage.clear();
  }

  async onLogin(form: NgForm) {
    console.log('form', form);
    this.submitted = true;
    if (form.valid) {
      const emailfilter = /^[\w._-]+[+]?[\w._-]+@[\w.-]+\.[a-zA-Z]{2,6}$/;
      if (!emailfilter.test(this.login.email)) {
        this._util.showToast(
          'Por favor ingrese un email valido',
          'danger',
          'bottom'
        );
        return false;
      }

      this._usuarioService
        .login(this.login.email, this.login.password)
        .subscribe(
          (usuario: Usuario) => {
            if (usuario.tipo_usuario == TipoUsuario.Administrador) {
              this.isLogin = true;
              this._navCtrl.navigateRoot(['/home-driver/tab1']);
            }
            if (usuario.tipo_usuario == TipoUsuario.Driver) {
              this.isLogin = true;
              this._navCtrl.navigateRoot(['/home-driver/tab1']);
            }
            if (usuario.tipo_usuario == TipoUsuario.Cliente) {
              this.isLogin = true;
              this._navCtrl.navigateRoot(['/home/tab1']);
            }
          },
          (e) => {
            this.isLogin = false;
          }
        );
    }
  }

  resetPass() {
    this._router.navigate(['forgot']);
  }
  register() {
    this._router.navigate(['login-choose-account']);
  }
}
