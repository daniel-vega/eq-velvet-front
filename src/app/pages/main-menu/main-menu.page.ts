import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from '../../services/usuario/usuario.service';
@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.page.html',
  styleUrls: ['./main-menu.page.scss'],
})
export class MainMenuPage implements OnInit {
  constructor(private router: Router, public _usuarioService: UsuarioService) {}

  ngOnInit() {}
  navigateToOrders() {
    this.router.navigate(['orders']);
  }
  navigateToInvoice() {
    this.router.navigate(['invoice']);
  }
  navigateToAgendaServicio() {
    this.router.navigate(['agenda-servicio-detail']);
  }
}
