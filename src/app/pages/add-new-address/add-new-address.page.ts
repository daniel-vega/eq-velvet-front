import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { Platform, IonSearchbar } from '@ionic/angular';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import {
  Geolocation,
  GeolocationOptions,
  Geoposition,
  PositionError,
} from '@ionic-native/geolocation/ngx';
import { NavController } from '@ionic/angular';
import { ApisService } from 'src/app/services/apis.service';
import { UtilService } from 'src/app/services/util.service';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
declare var google;
@Component({
  selector: 'app-add-new-address',
  templateUrl: './add-new-address.page.html',
  styleUrls: ['./add-new-address.page.scss'],
})
export class AddNewAddressPage implements OnInit {
  @ViewChild('map', { static: true }) mapEle: ElementRef;
  @ViewChild ('addres', {static: true}) input: IonSearchbar;
  map: any;
  marker: any;
  lat: any;
  lng: any;
  address: any;
  house: any = '';
  landmark: any = '';
  title: any = 'home';
  id: any;
  from: any;
  autocomplete: any;
  autocompleteItems: any;
  GoogleAutocomplete: any; 
  geocoder: any
  constructor(
    private platform: Platform,
    private androidPermissions: AndroidPermissions,
    private diagnostic: Diagnostic,
    public geolocation: Geolocation,
    private navCtrl: NavController,
    private api: DataService,
    private util: UtilService,
    private route: ActivatedRoute,
    public zone: NgZone,
  ) {
    this.geocoder = new google.maps.Geocoder;
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.autocomplete = {
      input: ''
    };
    this.autocompleteItems = [];
    this.route.queryParams.subscribe((data) => {
      console.log(data);
      if (data && data.from) {
        this.from = 'edit';
        const info = JSON.parse(data.data);
        console.log('da===>', info);
        this.address = info.address;
        this.house = info.house;
        this.id = info.id;
        this.landmark = info.landmark;
        this.lat = info.lat;
        this.lng = info.lng;
        this.loadmap(this.lat, this.lng, this.mapEle);
      } else {
        this.from = 'new';
        this.getLocation();
      }
    });
  }

  ngOnInit() {}

  getLocation() {
    this.platform.ready().then(() => {
      if (this.platform.is('android')) {
        this.androidPermissions
          .checkPermission(
            this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION
          )
          .then(
            (result) => console.log('Has permission?', result.hasPermission),
            (err) =>
              this.androidPermissions.requestPermission(
                this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION
              )
          );
        this.grantRequest();
      } else if (this.platform.is('ios')) {
        this.grantRequest();
      } else {
        this.geolocation
          .getCurrentPosition({
            maximumAge: 3000,
            timeout: 10000,
            enableHighAccuracy: false,
          })
          .then((resp) => {
            if (resp) {
              console.log('resp', resp);
              this.lat = resp.coords.latitude;
              this.lng = resp.coords.longitude;
              this.loadmap(
                resp.coords.latitude,
                resp.coords.longitude,
                this.mapEle
              );
              this.getAddress(this.lat, this.lng);
            }
          });
      }
    });
  }

  grantRequest() {
    this.diagnostic
      .isLocationEnabled()
      .then(
        (data) => {
          if (data) {
            this.geolocation
              .getCurrentPosition({
                maximumAge: 3000,
                timeout: 10000,
                enableHighAccuracy: false,
              })
              .then((resp) => {
                if (resp) {
                  console.log('resp', resp);
                  this.loadmap(
                    resp.coords.latitude,
                    resp.coords.longitude,
                    this.mapEle
                  );
                  this.getAddress(resp.coords.latitude, resp.coords.longitude);
                }
              });
          } else {
            this.diagnostic.switchToLocationSettings();
            this.geolocation
              .getCurrentPosition({
                maximumAge: 3000,
                timeout: 10000,
                enableHighAccuracy: false,
              })
              .then((resp) => {
                if (resp) {
                  console.log('ress,', resp);
                  this.loadmap(
                    resp.coords.latitude,
                    resp.coords.longitude,
                    this.mapEle
                  );
                  this.getAddress(resp.coords.latitude, resp.coords.longitude);
                }
              });
          }
        },
        (error) => {
          // console.log('errir', error);
        }
      )
      .catch((error) => {
        // console.log('error', error);
      });
  }

  loadmap(lat, lng, mapElement) {
    const location = new google.maps.LatLng(lat, lng);
    const style: any = [
      {
        featureType: 'all',
        elementType: 'all',
        stylers: [{ saturation: -100 }],
      },
    ];

    const mapOptions = {
      zoom: 15,
      scaleControl: false,
      streetViewControl: false,
      zoomControl: false,
      overviewMapControl: false,
      center: location,
      mapTypeControl: false,
      mapTypeControlOptions: {
        mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'Foodfire5'],
      },
    };
    this.map = new google.maps.Map(mapElement.nativeElement, mapOptions);
    var mapType = new google.maps.StyledMapType(style, { name: 'Grayscale' });
    this.map.mapTypes.set('Foodfire5', mapType);
    this.map.setMapTypeId('Foodfire5');
    this.addMarker(location);
  }

  getAddress(lat, lng) {
    const geocoder = new google.maps.Geocoder();
    const location = new google.maps.LatLng(lat, lng);
    geocoder.geocode({ location: location }, (results, status) => {
      console.log(results);
      this.address = results[0].formatted_address;
      this.lat = lat;
      this.lng = lng;
    });
  }

  addMarker(location) {
    console.log('location =>', location);
    const icon = {
      url: 'assets/icon/marker.png',
      scaledSize: new google.maps.Size(50, 50), // scaled size
    };
    this.marker = new google.maps.Marker({
      position: location,
      map: this.map,
      icon: icon,
      draggable: true,
      animation: google.maps.Animation.DROP,
    });

    google.maps.event.addListener(this.marker, 'dragend', () => {
      console.log(this.marker);
      this.getDragAddress(this.marker);
    });
  }

  getDragAddress(event) {
    const geocoder = new google.maps.Geocoder();
    const location = new google.maps.LatLng(
      event.position.lat(),
      event.position.lng()
    );
    geocoder.geocode({ location: location }, (results, status) => {
      console.log(results);
      this.address = results[0].formatted_address;
      this.lat = event.position.lat();
      this.lng = event.position.lng();
    });
  }
  submit() {
    localStorage.setItem('newAddress', this.address);
    localStorage.setItem('newLng', this.lng);
    localStorage.setItem('newLat', this.lat);
    this.navCtrl.back();
  }
  addAddress() {
    if (this.address === '' || this.landmark === '' || this.house === '') {
      // this.util.errorToast(this.util.translate('All Fields are required'));
      this.util.showToast('La dirección se ha guardado', 'success', 'bottom');
      return false;
    }
    console.log('call api');
    this.util.show();
    this.api.checkAuth2().subscribe(
      (data: any) => {
        console.log(data);

        if (data) {
          const id = this.util.makeid(10);
          const param = {
            id: id,
            uid: data.uid,
            address: this.address,
            lat: this.lat,
            lng: this.lng,
            title: this.title,
            house: this.house,
            landmark: this.landmark,
          };
          this.api.addNewAddress(data.uid, id, param).subscribe(
            (data) => {
              this.util.hide();
              // this.util.showToast(
              //   this.util.translate('succesfully added address'),
              //   'success',
              //   'bottom'
              // );
              this.util.showToast(
                'La dirección se ha guardado',
                'success',
                'bottom'
              );
              this.navCtrl.back();
            },
            (error) => {
              this.util.hide();
              console.log(error);
              this.util.errorToast(this.util.translate('Something went wrong'));
            }
          ); //.catch(error => {
          //   this.util.hide();
          //   console.log(error);
          //   this.util.errorToast(this.util.translate('Something went wrong'));
          // });
        } else {
          this.util.hide();
          this.util.errorToast(this.util.translate('Something went wrong'));
          this.navCtrl.navigateRoot(['tabs']);
        }
      },
      (error) => {
        this.util.hide();
        console.log(error);
        this.util.errorToast(this.util.translate('Something went wrong'));
        this.navCtrl.navigateRoot(['tabs']);
      }
    ); //.catch(error => {
    //   this.util.hide();
    //   console.log(error);
    //   this.util.errorToast(this.util.translate('Something went wrong'));
    //   this.navCtrl.navigateRoot(['tabs']);
    // });
  }

  updateAddress() {
    if (this.address === '' || this.landmark === '' || this.house === '') {
      // this.util.errorToast(this.util.translate('All Fields are required'));
      this.util.showToast('La dirección se ha guardado', 'success', 'bottom');
      return false;
    }
    const param = {
      id: this.id,
      uid: localStorage.getItem('uid'),
      address: this.address,
      lat: this.lat,
      lng: this.lng,
      title: this.title,
      house: this.house,
      landmark: this.landmark,
    };
    this.util.show();
    this.api
      .updateAddress(localStorage.getItem('uid'), this.id, param)
      .subscribe((data) => {
        this.util.hide();
        this.util.showToast('Address updated', 'success', 'bottom');
        this.navCtrl.back();
      }); //.catch(error => {
    //     this.util.hide();
    //     console.log('error', error);
    //     this.util.errorToast(this.util.translate('Something went wrong'));
    //   });
  }
  search(event: any){
    if (this.autocomplete.input == '') {
      this.autocompleteItems = [];
      return;
    }
  
      this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete.input},
        (predictions, status) => {
          this.autocompleteItems = [];
          if(predictions){
            this.zone.run(() => {
              predictions.forEach((prediction) => {
                this.autocompleteItems.push(prediction);
              });
            });
          }
      });
      this.address = undefined;
  }
  selectSearchResult(item){
    
    this.autocompleteItems = [];

    this.geocoder.geocode({'placeId': item.place_id}, (results, status) => {
      if(status === 'OK' && results[0]){

       
        this.loadmap(
          results[0].geometry.location.lat(),
          results[0].geometry.location.lng(),
          this.mapEle
        );
        //console.log(results[0])
       
         this.address = item.description;
         //this.input.value = item.description;
         this.autocompleteItems = [];


      } 
      
    })
    this.autocomplete.input = '';
  }
  
}
