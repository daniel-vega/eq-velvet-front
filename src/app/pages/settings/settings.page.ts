import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { UsuarioService } from '../../services/usuario/usuario.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  seg_id = 1;
  name: any;
  photo: any = 'assets/img-compartidas/user.jpg';
  email: any;
  reviews: any = [];
  id: any;

  constructor(private router: Router, public _usuarioService: UsuarioService) {}

  ngOnInit() {}
  goToEditProfile() {
    this.router.navigate(['/edit-profile']);
  }

  goToadminDirecciones() {
    const navData: NavigationExtras = {
      queryParams: {
        from: 'accont',
      },
    };
    this.router.navigate(['choose-address'], navData);
  }
  goToAdminRepartidores() {
    this.router.navigate(['choose-driver']);
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['login']);
  }
}
