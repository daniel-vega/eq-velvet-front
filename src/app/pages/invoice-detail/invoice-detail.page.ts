import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { UtilService } from 'src/app/services/util.service';
import { AlertController, NavController } from '@ionic/angular';
import { OrdenService } from '../../services/orden/orden.service';
import { ordenDetalle } from '../../interfaces/IOrdenes.interface';

@Component({
  selector: 'app-invoice-detail',
  templateUrl: './invoice-detail.page.html',
  styleUrls: ['./invoice-detail.page.scss'],
})
export class InvoiceDetailPage implements OnInit {
  id: any;
  ordersDetalle: ordenDetalle[] = <ordenDetalle[]>[];
  total: number = 0;
  loaded: boolean;
  idDireccion: number;
  idOrdenDetalle: number;
  coupon;
  dicount;
  serviceTax;
  paid;
  time;
  deliveryAddress;
  servicioDomicilio = 100;
  constructor(
    private _route: ActivatedRoute,
    private _OrdenService: OrdenService,
    private _router: Router,
    private _util: UtilService
  ) {
    this.loaded = false;
  }

  ngOnInit() {
    this._route.queryParams.subscribe((data) => {
      debugger;
      if (data.hasOwnProperty('id_orden')) {
        this.idOrdenDetalle = data.id_orden;
        this.idDireccion = data.idDireccion;
        this.getOrder(this.idOrdenDetalle, this.idDireccion);
      }
    });
  }

  getOrder(idOrdenDetalle, idDireccion) {
    this._OrdenService
      .getOrdenesDetalleByID(idOrdenDetalle, idDireccion)
      .subscribe(
        (data) => {
          this.loaded = true;
          if (data) {
            this.ordersDetalle = data.detalle;
            debugger;
            for (const order of this.ordersDetalle) {
              this.total = this.total + Number(order.subtotal);
            }

            debugger;
          }
        },
        (error) => {
          console.log('error in orders', error);
          this.loaded = true;
        }
      );
  }

  chat() {
    this._router.navigate(['inbox']);
  }

  getCurrency() {
    return this._util.getCurrecySymbol();
  }
}
