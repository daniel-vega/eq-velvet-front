import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-agenda-servicio',
  templateUrl: './agenda-servicio.page.html',
  styleUrls: ['./agenda-servicio.page.scss'],
})
export class AgendaServicioPage implements OnInit {
  disabled = true;
  dateIon: string;
  fechaFinalIon: string;
  days;
  fechaSeleccionada: string;
  horaSeleccionada: string;
  constructor(private router: Router, private navCtrl: NavController) {
    this.days = new Days();
    // this.db
    //   .collection(`events`)
    //   .snapshotChanges()
    //   .subscribe((colSnap) => {
    //     this.eventSource = [];
    //     colSnap.forEach((snap) => {
    //       let event: any = snap.payload.doc.data();
    //       event.id = snap.payload.doc.id;
    //       event.startTime = event.startTime.toDate();
    //       event.endTime = event.endTime.toDate();
    //       console.log(event);
    //       this.eventSource.push(event);
    //     });
    //   });
  }
  ngOnInit() {
    this.getDate();
  }
  getDate() {
    let dateNew = new Date();
    const dateTimeFormat = new Intl.DateTimeFormat('en', {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
    });
    const [
      { value: month },
      ,
      { value: day },
      ,
      { value: year },
    ] = dateTimeFormat.formatToParts(dateNew);
    this.dateIon = year + '-' + month + '-' + day;
    this.fechaFinalIon =
      year + '-' + month + '-' + (Number(day) + 7).toString();
  }
  onCurrentDateChanged(event) {
    // console.log(event);
    this.fechaSeleccionada = event;
  }
  selectBotton(event) {
    this.horaSeleccionada = event;
    this.disabled = false;
  }

  navigateToAgendaServicio() {
    this.router.navigate(['agenda-servicio-detail']);
  }
  back() {
    this.router.navigate(['home/tab1']);
  }
}

export class Days {
  public customDayShortNames = [];
  public customMonthShortNames = [];
  constructor() {
    this.customDayShortNames = [
      'Domingo',
      'Lunes',
      'Martes',
      'Miércoles',
      'Jueves',
      'Viernes',
      'Sábado',
    ];
    this.customMonthShortNames = [
      'Enero',
      'Febrero',
      'Marzo',
      'Abril',
      'Mayo',
      'Junio',
      'Julio',
      'Agosto',
      'Septiembre',
      'Octubre',
      'Noviembre',
      'Diciembre',
    ];
  }
}
