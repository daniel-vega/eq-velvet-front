import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgendaServicioPage } from './agenda-servicio.page';

const routes: Routes = [
  {
    path: '',
    component: AgendaServicioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgendaServicioPageRoutingModule {}
