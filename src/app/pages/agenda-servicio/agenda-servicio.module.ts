import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgendaServicioPageRoutingModule } from './agenda-servicio-routing.module';

import { AgendaServicioPage } from './agenda-servicio.page';
import { NgCalendarModule } from 'ionic2-calendar';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgCalendarModule,
    AgendaServicioPageRoutingModule,
  ],
  declarations: [AgendaServicioPage],
})
export class AgendaServicioPageModule {}
