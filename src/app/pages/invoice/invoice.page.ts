import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { IonList, IonSegment } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { OrdenService } from 'src/app/services/orden/orden.service';
import { Ordenes, ordenModelPost } from '../../interfaces/IOrdenes.interface';
import { UsuarioService } from '../../services/usuario/usuario.service';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.page.html',
  styleUrls: ['./invoice.page.scss'],
})
export class InvoicePage implements OnInit {
  @ViewChild(IonSegment, { static: true }) segment: IonSegment;
  @ViewChild('lista', { static: false }) lista: IonList;
  publisher = '';
  ordenes: Observable<any>;

  constructor(
    private _ordenService: OrdenService,
    private router: Router,
    public _usuarioService: UsuarioService
  ) {}

  ngOnInit() {
    this.segment.value = 'todos';
    this.ordenes = this._ordenService.getAllOrdenesByID(
      this._usuarioService.usuario.id
    );
    this.ordenes.subscribe((data) => {});
  }

  segmentChanged(event) {
    const valorSegmento = event.detail.value;

    if (valorSegmento === 'todos') {
      this.publisher = '';
      return;
    }

    this.publisher = 'caro';

    console.log(valorSegmento);
  }

  favorite(user) {
    console.log('favorite', user);
    this.lista.closeSlidingItems();
  }
  share() {
    this.lista.closeSlidingItems();
  }
  borrar(user) {
    console.log('borrar', user);
    this.lista.closeSlidingItems();
  }
  invoiceDetail(id_orden, idDireccion) {
    debugger;
    const navData: NavigationExtras = {
      queryParams: {
        id_orden: id_orden,
        idDireccion: idDireccion,
      },
    };

    this.router.navigate(['invoice-detail'], navData);
  }
}
