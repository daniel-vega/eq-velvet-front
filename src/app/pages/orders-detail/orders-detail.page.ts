import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { UtilService } from 'src/app/services/util.service';
import {
  ordenDetalle,
  ordenModelPost,
} from 'src/app/interfaces/IOrdenes.interface';

@Component({
  selector: 'app-orders-detail',
  templateUrl: './orders-detail.page.html',
  styleUrls: ['./orders-detail.page.scss'],
})
export class OrdersDetailPage implements OnInit {
  id: any;
  grandTotal: any;
  orders: any[] = [];
  serviceTax: any;
  status: any;
  time: any;
  total: any = 0;
  uid: any;
  address: any;
  restName: any;
  deliveryAddress: any;
  paid: any;
  restPhone: any;
  coupon: boolean = false;
  dicount: any;
  dname: any;
  orderData: any;
  loaded: boolean;
  restFCM: any;
  driverFCM: any;
  dId: any;
  orderView: ordenModelPost = <ordenModelPost>{};
  ordersDetalle: ordenDetalle[];
  servicioDomicilio = 100;
  constructor(private router: Router, private util: UtilService) {
    this.orderView = JSON.parse(localStorage.getItem('orderView'));
    this.ordersDetalle = this.orderView[0].orden_detalle;
    for (const order of this.ordersDetalle) {
      this.total = this.total + Number(order.subtotal);
    }
    debugger;
    this.loaded = true;
  }

  ngOnInit() {
    // this.route.queryParams.subscribe((data) => {
    //   console.log('data=>', data);
    //   if (data.hasOwnProperty('id')) {
    //     this.id = data.id;
    //     this.getOrder();
    //   }
    // });
  }

  getOrder() {}

  trackMyOrder() {
    const navData: NavigationExtras = {
      queryParams: {
        id: this.id,
      },
    };
    this.router.navigate(['/tracker'], navData);
  }
  call() {
    if (this.restPhone) {
      window.open('tel:' + this.restPhone);
    }
  }

  chat() {
    this.router.navigate(['inbox']);
  }

  changeStatus() {}
  getCurrency() {
    return this.util.getCurrecySymbol();
  }
}
