import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';
import { UtilService } from 'src/app/services/util.service';
import { NavController } from '@ionic/angular';
import * as moment from 'moment';
import { AlertsComponent } from '../../components/alerts/alerts.component';
import {
  servicios,
  ordenModelPost,
  ordenDetalle,
} from '../../interfaces/IOrdenes.interface';
import { UsuarioService } from '../../services/usuario/usuario.service';
import { OrdenService } from '../../services/orden/orden.service';
@Component({
  selector: 'app-payments',
  templateUrl: './payments.page.html',
  styleUrls: ['./payments.page.scss'],
})
export class PaymentsPage implements OnInit {
  totalPrice: any = 0;
  totalItem: any = 0;
  serviceTax: any = 0;
  servicios: servicios[] = [];
  ordenPost: ordenModelPost = <ordenModelPost>{};
  constructor(
    private navCtrl: NavController,
    private _Alerts: AlertsComponent,
    private _usuario: UsuarioService,
    private _orden: OrdenService
  ) {}

  async ngOnInit() {
    this.validate();
  }

  validate() {
    const cart = localStorage.getItem('cartService');
    try {
      if (
        cart &&
        cart !== 'null' &&
        cart !== undefined &&
        cart !== 'undefined'
      ) {
        this.servicios = JSON.parse(localStorage.getItem('cartService'));
        console.log('cart', JSON.stringify(this.servicios));
        this.calculate();
      } else {
        this.servicios = [];
      }
    } catch (error) {
      console.log(error);
      this.servicios = [];
    }
    return true;
  }

  async calculate() {}

  placeOrder() {
    this._Alerts
      .confirmLeave('¿Estas seguro?', 'Se iniciara un nuevo servicio')
      .then((res) => {
        if (res) {
          console.log('ok');
          this.createOrder();
          this.navCtrl.navigateRoot(['home/tab1']);
          this._Alerts.presentToast('Tu servicio se ha creado correctamente');
        }
      });
  }

  async createOrder() {
    let ordenDetalles: ordenDetalle[] = <ordenDetalle[]>[];
    let item = this.servicios.filter((x) => x.cantidad > 0);
    if (item.length > 0) {
      item.forEach((element) => {
        this.totalItem = this.totalItem + element.cantidad;
        this.totalPrice =
          this.totalPrice +
          parseFloat(element.precio.toString()) *
            parseInt(element.cantidad.toString());
        let detalle: ordenDetalle = <ordenDetalle>{
          id_servicio: element.id,
          cantidad: element.cantidad,
          subtotal: element.precio * element.cantidad,
        };
        ordenDetalles.push(detalle);
      });
    }

    this.ordenPost.gastos_envio = 100;
    this.ordenPost.descuento = 0;
    this.ordenPost.total = this.totalPrice;
    this.ordenPost.iva = 16;
    this.ordenPost.fecha_orden = new Date();
    this.ordenPost.subtotal = this.totalPrice;
    this.ordenPost.estatus = 'En proceso';
    this.ordenPost.estatus_pago = 'Pendiente';
    this.ordenPost.id_direccion = 1;
    this.ordenPost.id_driver = 2;
    this.ordenPost.id_usuario = this._usuario.usuario.id;
    this.ordenPost.id_lavanderia = null;
    this.ordenPost.id_cupon = null;
    let OrdenPost = { orden: this.ordenPost, orden_detalle: ordenDetalles };
    this.enviarOrden(OrdenPost);
  }

  enviarOrden(ordenPost) {
    this._orden.crearOrden(ordenPost).subscribe((data) => {
      debugger;
    });
  }
}
