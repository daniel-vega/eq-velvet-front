import { TipoUsuario } from './enums';
// export interface Usuario {
//   id: number;
//   email: string;
//   apellidos: string;
//   contrasenia: string;
//   createdAT: Date;
//   updateAT: Date;
//   nombre: string;
//   apodo: string;
//   latitud: number;
//   longitud: number;
//   img_cover: string;
//   caballerango_id: number;
//   tipo_usuario: TipoUsuario;
// }
export interface Usuario {
  id: number;
  email: number;
  contraseña: string;
  nombre: string;
  apellido: string;
  apodo: string;
  idEstatusUsuario: number;
  tipo_usuario: TipoUsuario;
  latitud: number;
  longitud: number;
  imgCover: string;
  ciudad: number;
}
