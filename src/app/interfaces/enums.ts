export enum PagoEstatus {
  NoPagado = 0,
  Pagado = 1,
  Pendiente = 2,
}
export enum PedidoEstatus {
  Entregado = 0,
  Pagado = 1,
  Pendiente = 2,
}

export enum ServiciosCategorias {
  Caballos = 0,
  Mascotas = 1,
}
export enum EstatusUsuario {
  Driver = 0,
  Cliente = 1,
}

export enum TipoUsuario {
  Driver = 'driver',
  Cliente = 'cliente',
  Administrador = 'administrador',
}
