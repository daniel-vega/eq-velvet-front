import { Usuario } from './IUsuarios.interface';
export interface Ordenes {
  id: number;
  gastos_envio: number;
  descuento: number;
  total: string;
  iva: string;
  fecha_orden: string;
  subtotal: number;
  estatus: number;
  estatus_pago: number;
}
export interface ordenModelPost {
  id: number;
  gastos_envio: number;
  descuento: number;
  total: number;
  iva: number;
  fecha_orden: Date;
  subtotal: number;
  estatus: string;
  estatus_pago: string;
  createdAt: Date;
  updatedAt: Date;
  id_direccion: number;
  id_metodo_pago: number;
  id_usuario: number;
  id_driver: number;
  id_lavanderia: number;
  id_cupon: number;
  orden_detalle: ordenDetalle[];
  usuario: Usuario;
}
export interface direcciones {
  id: number;
  usuarioID: number;
  titulo: string;
  numeroCasa: string;
  latitud: string;
  longitud: string;
}
export interface ordenDetalle {
  subtotal: number;
  cantidad: number;
  createdAt: Date;
  updatedAt: Date;
  id_servicio: number;
  id_orden: number;
  servicio: servicios;
}
export interface servicios {
  id: number;
  nombre: number;
  descripcion: number;
  precio: number;
  estatus: number;
  img_cover: number;
  createdAt: number;
  updatedAt: number;
  id_categoria: number;
  cantidad: number;
}
export interface lavanderia {
  id: number;
  ciudad: number;
  latitud: number;
  longitud: number;
  horaCirra: number;
  horaApertura: number;
  telefono: number;
  estatus: number;
  imgCover: number;
  nombre: number;
}
