export interface register {
  email: string;
  full_name: string;
  password: string;
  confirmPassword: string;
  apellidos: string;
  apodo: string;
}
