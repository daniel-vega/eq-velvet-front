import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./index/index.module').then((m) => m.IndexPageModule),
  },
  {
    path: '',
    loadChildren: () =>
      import('./home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: 'home-driver',
    loadChildren: () =>
      import('./home-driver/home-driver.module').then(
        (m) => m.HomeDriverPageModule
      ),
  },
  {
    path: 'estado-servicio',
    loadChildren: () =>
      import('./pages/estado-servicio/estado-servicio.module').then(
        (m) => m.EstadoServicioPageModule
      ),
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
