import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndexPage } from './index.page';
import { RegisterPageModule } from '../pages/register/register.module';

const routes: Routes = [
  // {
  //   path: '',
  //   loadChildren: () =>
  //     import('../pages/welcome/welcome.module').then(
  //       (m) => m.WelcomePageModule
  //     ),
  // },
  {
    path: 'login',
    loadChildren: () =>
      import('../pages/login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },

  {
    path: 'login-choose-account',
    loadChildren: () =>
      import('../pages/login-choose-account/login-choose-account.module').then(
        (m) => m.LoginChooseAccountPageModule
      ),
  },
  {
    path: 'register',
    loadChildren: () =>
      import('../pages/register/register.module').then(
        (m) => m.RegisterPageModule
      ),
  },
  {
    path: 'forgot',
    loadChildren: () =>
      import('../pages/forgot/forgot.module').then((m) => m.ForgotPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IndexPageRoutingModule {}
