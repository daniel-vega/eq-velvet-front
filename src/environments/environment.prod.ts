export const environment = {
  production: true,
  URL_ENDPOINT: 'http://www.caballo.est177.edu.mx/api',
  general: {
    symbol: '$',
    code: 'USD',
  },
};
